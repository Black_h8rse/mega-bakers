<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
     * Author: Hassan Raza
     * Google: @blackh8rse instagram
     * Gmail : blackh8rse@gmail.com
     * Skype : hassan6xaimii
 */

//                                                                      ADMIN HELPER FUNCTIONS

if ( ! function_exists('admin_url'))
{
    function admin_url()
    {
        $CI = get_instance();
        return $CI->config->item('admin_url');
    }
}

if ( ! function_exists('admin_controller'))
{
    function admin_controller()
    {
        $CI = get_instance();
        return $CI->config->item('admin_controller');
    }

}

if ( ! function_exists('show_admin404'))
{
    function show_admin404()
    {
        $CI = get_instance();
        return $CI->load->view('Common/error/admin_error_page');
    }
}

if ( ! function_exists('email'))
{
    function email($from, $to, $subject, $message)
    {
        $CI = get_instance();
        $CI->email->from($from);
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($message);

        return $CI->email->send();
    }
}

//                                                                      END ADMIN HELPER FUNCTIONS

//                                                                      SESSIONS 

if ( ! function_exists('get_session'))
{
    function get_session($session_name)
    {
        $CI = get_instance();
        return $CI->session->userdata($session_name);
    }

}
if ( ! function_exists('set_session'))
{
    function set_session($session_name, $value)
    {
        $CI = get_instance();
        return $CI->session->set_userdata($session_name, $value);
    }

}
if ( ! function_exists('unset_session'))
{
    function unset_session($session_name)
    {
        $CI = get_instance();
        return $CI->session->unset_userdata($session_name);
    }
}

//                                                                      END SESSIONS

                                                                                               
//                                                                      GENERAL FUNCTIONS

if ( ! function_exists('show_404'))
{
    function show_404()
    {
        $CI = get_instance();
        return $CI->load->view('common/error_page');
    }
}
if ( ! function_exists('get_table'))
{
    function get_table($table_name = '')
    {
        $CI = get_instance();
        $CI->db->select('*');
        $CI->db->from($table_name);
        $query = $CI->db->get()->result_array();
        return $query;
    }
}
if ( ! function_exists('get_name'))
{
    function get_name( $table_name = '', $column_name = '', $value = '',$return = '' )
    {
        $CI = get_instance();
        $CI->db->select( '*' );
        $CI->db->from( $table_name );
        $CI->db->where( $column_name, $value );
        if( !empty($return) ) {
            $query = $CI->db->get()->row_array();
            if(@$query['name']) {
                return $query['name'];
            } else {
                return $query[$return];
            }
        }else {
            return $CI->db->get()->result_array();
        }
    }
}
if ( ! function_exists('get_user_name'))
{
    function get_user_name( $user_id = '')
    {
        $CI = get_instance();
        $CI->db->select( '*' );
        $CI->db->from( 'users' );
        $CI->db->where( 'id', $user_id );
        $query = $CI->db->get()->row_array();
        return $query['f_name'].' '.$query['l_name'];
    }
}
if ( ! function_exists('sub_cats'))
{
    function sub_cats( $cat_id = '' )
    {
        $CI = get_instance();
        $CI->db->select( '*' );
        $CI->db->from( 'sub_categories' );
        $CI->db->where( 'cat_id', $cat_id );
        return $CI->db->get()->result_array();
    }
}
if ( ! function_exists('get_data'))
{
    function get_data( $id, $table, $where = "", $select = '', $limit = '', $order_by = "", $order = "", $joins = '', $group_by = "") {
        $CI = get_instance();
        if ($group_by != "") {
            $CI->db->group_by($group_by);
        }

        if ($limit != "") {
            $CI->db->limit($limit[0], $limit[1]);
        }

        if (!is_array($order_by)) {
            if ($order_by != "") {
                if ($order != "") {
                    $CI->db->order_by($order_by, $order);
                } else {
                    $CI->db->order_by($order_by, "desc");
                }
            }
        } else {
//            print_r($order_by);exit;
            foreach ($order_by as $key => $orders) {
                $CI->db->order_by($orders[0], $orders[1]);
//                print_r($orders);
            }
//            exit;
        }


        if ($joins != "") {
            foreach ($joins as $join) {
                $CI->db->join($join[0], $join[1], $join[2]);
            }
        }


        if ($where != "") {
            $CI->db->where($where);
        }


        if ($id != "") {
            $CI->db->where('id', $id);
        }

        if (is_array($select)) {
            foreach ($select as $key => $value) {
                $CI->db->select($value);
            }

        } else {
            $CI->db->select($select);
        }

        $result = $CI->db->get($table)->result_array();

        return $result;
    }
}

//                                                                      END GENERAL FUNCTIONS

//                                                                      START API FUNCTIONS
//                                                                      START API FUNCTIONS

if (!function_exists('get_user_by_email'))
{
    function get_user_by_email($email)
    {
        $CI = get_instance();
        $CI->db->select('*');
        $CI->db->where(['email'=>$email])->from("users");
        $query = $CI->db->get()->row_array();
        return $query;
    }
}
if (!function_exists('get_user_by_username'))
{
    function get_user_by_username($username)
    {
        $CI = get_instance();
        $CI->db->select('*');
        $CI->db->where(['username'=>$username])->from("users");
        $query = $CI->db->get()->row_array();
        return $query;
    }
}

if (!function_exists('check_and_generate_key'))
{
    function check_and_generate_key($table_name,$column_name)
    {
        $CI  = get_instance();
        $key = generateRandomString();
        $CI->db->select('*');
        $CI->db->where([$column_name=>$key])->from($table_name);
        $query = $CI->db->get()->row_array();
        if (!empty($query)) {
            check_and_generate_key($table_name);
        }else {
            return $key;
        }
    }
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if ( ! function_exists('get_counter'))
{
    function get_counter($id, $column, $table) {
        $CI = & get_instance();
        $CI->db->select($column);
        $CI->db->from($table);
        $CI->db->where('id' , $id);
        $query = $CI->db->get()->row_array();
        return $query[$column];

    }
}


if ( ! function_exists('get_count'))
{
    function get_count($table, $type = '') {
        $CI = & get_instance();
        $CI->db->select("count(id) as total");
        if(get_session('admin_id') != '1') {
            $CI->db->where('created_by', get_session('admin_id'));
        }
        $CI->db->from($table);

        
        if ($type!='') {
            $CI->db->where('type',$type);
        }

        $result = $CI->db->get()->row_array();
        return  $result['total'];
    }
}

if ( ! function_exists('get_fvrt_pro_array'))
{
    function get_fvrt_pro_array($user_id) {
        $CI = & get_instance();
        $CI->db->select("*");
        $CI->db->from('product_fvrt');
        $CI->db->group_by('product_id');
        $CI->db->where('user_id',$user_id);
        $products_array = $CI->db->get()->result_array();
        foreach ($products_array as $key => $value) {
            $CI->db->select("id,name,image,img_alt");
            $CI->db->from('products');
            $CI->db->where('id',$value['product_id']);
            $products_array[$key] = $CI->db->get()->row_array();   
        }
        return $products_array;
    }
}
if ( ! function_exists('get_product_by_id'))
{
    function get_product_by_id($pro_id) {
        $CI = & get_instance();
        $CI->db->select("*");
        $CI->db->from('products');
        $CI->db->where('id',$pro_id);
        return $CI->db->get()->row_array();
    }
}
if ( ! function_exists('remove_fvrt_product_by_id'))
{
    function remove_fvrt_product_by_id($pro_id,$user_id) {
        $CI = & get_instance();
        $CI->db->where('user_id', $user_id);
        $CI->db->where('product_id', $pro_id);
        $query = $CI->db->delete('product_fvrt');
        return $CI->db->affected_rows();
    }
}
if ( ! function_exists('search_by_text'))
{
    function search_by_text($product_name) {
        $CI = & get_instance();
        $CI->db->select("*");
        $CI->db->from('products');
        $CI->db->like('name', $product_name);
        return $CI->db->get()->result_array();
    }
}
if ( ! function_exists('search_by_qr'))
{
    function search_by_qr($qr_code) {
        $CI = & get_instance();
        $CI->db->select("*");
        $CI->db->from('products');
        $CI->db->where('QR_code', $qr_code);
        return $CI->db->get()->result_array();
    }
}

if ( ! function_exists('get_meta'))
{
    function get_meta($table,$column_name='',$value='',$meta_key='',$meta_value='') {
        $CI = & get_instance();
        $CI->db->select("*");
        $CI->db->from($table);
        if( !empty($column_name) ) {
            $CI->db->where($column_name, $value);
        }
        if( !empty($meta_key) ) {
            $CI->db->where($meta_key, $meta_value);
            $array = $CI->db->get()->result_array();
            return $array['0']['meta_value'];
        }
        return $CI->db->get()->result_array();
    }
}



//                                                                      END API FUNCTIONS


//                                                                          COMPARE IMAGE

if ( ! function_exists('mimeType'))
{
    function mimeType($i)
    {
        /*returns array with mime type and if its jpg or png. Returns false if it isn't jpg or png*/
        $mime   = getimagesize($i);
        $return = array($mime[0],$mime[1]);
      
        switch ($mime['mime'])
        {
            case 'image/jpeg':
                $return[] = 'jpg';
                return $return;
            case 'image/png':
                $return[] = 'png';
                return $return;
            default:
                return false;
        }
    } 
} 
if ( ! function_exists('createImage'))
{   
    function createImage($i)
    {
        /*retuns image resource or false if its not jpg or png*/
        $mime = mimeType($i);
      
        if($mime[2] == 'jpg')
        {
            return imagecreatefromjpeg ($i);
        } 
        else if ($mime[2] == 'png') 
        {
            return imagecreatefrompng ($i);
        } 
        else 
        {
            return false; 
        } 
    }
}
if ( ! function_exists('resizeImage'))
{
    
    function resizeImage($i,$source)
    {
        /*resizes the image to a 8x8 squere and returns as image resource*/
        $mime = mimeType($source);
      
        $t = imagecreatetruecolor(8, 8);
        
        $source = createImage($source);
        
        imagecopyresized($t, $source, 0, 0, 0, 0, 8, 8, $mime[0], $mime[1]);
        
        return $t;
    }
}
if ( ! function_exists('colorMeanValue'))
{
    
    function colorMeanValue($i)
    {
        /*returns the mean value of the colors and the list of all pixel's colors*/
        $colorList = array();
        $colorSum = 0;
        for($a = 0;$a<8;$a++)
        {
        
            for($b = 0;$b<8;$b++)
            {
            
                $rgb = imagecolorat($i, $a, $b);
                $colorList[] = $rgb & 0xFF;
                $colorSum += $rgb & 0xFF;
                
            }
            
        }
        
        return array($colorSum/64,$colorList);
    }
}
if ( ! function_exists('bits'))
{
    
    function bits($colorMean)
    {
        /*returns an array with 1 and zeros. If a color is bigger than the mean value of colors it is 1*/
        $bits = array();
         
        foreach($colorMean[1] as $color){$bits[]= ($color>=$colorMean[0])?1:0;}

        return $bits;

    }
}
if ( ! function_exists('compare'))
{
    
    function compare($a,$b)
    {
        /*main function. returns the hammering distance of two images' bit value*/
        $i1 = createImage($a);
        $i2 = createImage($b);
        
        if(!$i1 || !$i2){return false;}
        
        $i1 = resizeImage($i1,$a);
        $i2 = resizeImage($i2,$b);
        
        imagefilter($i1, IMG_FILTER_GRAYSCALE);
        imagefilter($i2, IMG_FILTER_GRAYSCALE);
        
        $colorMean1 = colorMeanValue($i1);
        $colorMean2 = colorMeanValue($i2);
        
        $bits1 = bits($colorMean1);
        $bits2 = bits($colorMean2);
        
        $hammeringDistance = 0;
        
        for( $a = 0; $a<64; $a++ )
        {
            if($bits1[$a] != $bits2[$a])
            {
                $hammeringDistance++;
            }
            
        }
        return $hammeringDistance;
    }
}


//                                                                          COMPARE IMAGE END