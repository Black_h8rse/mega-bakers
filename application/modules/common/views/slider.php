<div class="slider-parallax-inner">
    <div class="swiper-container swiper-parent">
        <div class="swiper-wrapper">
            <?php $slider1=get_sliders($details['id'],1,'',1);
            if (!empty($slider1)){ ?>
                <div class="swiper-slide banner-slide1" style="background-image: url('<?php echo base_url(); ?>assets/area/slider/<?php echo $slider1[0]['image']; ?>');">
                    <div class="container clearfix">
                        <div class="slider-caption">
                            <p data-caption-animate="fadeInUp"> <?php echo $slider1[0]['heading']; ?></p>
                            <p data-caption-animate="fadeInUp" data-caption-delay="200"><?php echo $slider1[0]['sub_heading']; ?></p>
                        </div>
                    </div>
                </div>
                <?php $sliders=get_sliders($details['id'],2,$slider1[0]['id']); }else{
                $sliders=get_sliders($details['id'],3); }
            foreach ($sliders as $slider){ ?>
                <?php if ($slider['media_type']==1){ ?>
                    <div class="swiper-slide banner-slide<?php echo $slider['id'] ?>" style="background-image: url('<?php echo base_url(); ?>assets/area/slider/<?php echo $slider['image']; ?>');">
                        <div class="container clearfix">
                            <div class="slider-caption">
                                <p data-caption-animate="fadeInUp"><?php echo $slider['heading']; ?></p>
                                <p data-caption-animate="fadeInUp" data-caption-delay="200"><?php echo $slider['sub_heading'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php }elseif ($slider['media_type']==2){ ?>
                    <div class="swiper-slide">
                        <div class="container clearfix">
                            <div class="slider-caption">
                                <p data-caption-animate="fadeInUp"><?php echo $slider['heading']; ?></p>
                                <p data-caption-animate="fadeInUp" data-caption-delay="200"><?php echo $slider['sub_heading'] ?></p>
                            </div>
                        </div>
                        <div class="video-wrap">
                            <video loop autoplay muted>
                                <?php $slider_video = get_video($slider['video']); ?>
                                <source data-src='<?php echo base_url('assets/uploads/').$slider_video['file']; ?>' type='video/mp4' />
                            </video>
                            <div class="video-overlay"></div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
        <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
        <div id="slide-number"><div id="slide-number-current"></div><span>/</span><div id="slide-number-total"></div></div>
    </div>
</div>


