<?php
$r_class    = $this->router->fetch_class();
$r_method   = $this->router->fetch_method();
?>

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" style="width: 50px;" src="<?php echo base_url(); ?>assets/logo.png"/>
                    <span data-toggle="dropdown" class="dropdown-toggle admin-name">
                        <span class="block m-t-xs font-bold">
                            Name: <?php echo get_session('admin_username'); ?>  
                        </span>
                        <span class="text-muted text-xs block">
                            Email: <?php echo get_session('admin_email'); ?>
                        </span>
                    </span>
                </div>
                <div class="logo-element">
                    Mega Bakers
                </div>
            </li>

            <li class="<?php if($r_class == 'admin' && ($r_method == 'dashboard' || $r_method == 'index')) { ?> active <?php } ?>">
                <a href="<?php echo admin_url(); ?>dashboard"><i class="fas fa-boxes"></i> <span class="nav-label">Dashboard (Not Completed yet!)</span></a>
            </li>

            <?php if(get_session('admin_type') == '1' || get_session('admin_type') == '2') { ?>
                <li class="<?php if($r_class == 'users' && ($r_method == 'users' || $r_method == 'index')) { ?> active <?php } ?>">
                    <a href="<?php echo admin_url(); ?>users"><i class="fa fa-user"></i> <span class="nav-label">Users</span></a>
                </li>
            <?php } ?>

            <?php if(get_session('admin_type') == '1' || get_session('admin_type') == '2' || get_session('admin_type') == '3') { ?> 
                <li class="<?php if($r_class == 'warehouses' && ($r_method == 'warehouses' || $r_method == 'index' || $r_method == 'products'|| $r_method == 'product_transfer')) { ?> active <?php } ?>">
                    <a href="javascript:void(0)"><i class="fas fa-warehouse"></i> <span class="nav-label">Warehouses</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?php if($r_class == 'warehouses' && ($r_method == 'index')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>warehouses">Warehouses</a></li>
                        <li class="<?php if($r_class == 'warehouses' && ($r_method == 'products')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>warehouses/products">Warehouse Products</a></li>
                        <li class="<?php if($r_class == 'warehouses' && ($r_method == 'product_transfer')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>warehouses/product_transfer">Transfer History</a></li>
                    </ul>
                </li>
            <?php } ?>

            <?php if(get_session('admin_type') == '1' || get_session('admin_type') == '2' || get_session('admin_type') == '4') { ?>
                <li class="<?php if($r_class == 'stores' && ($r_method == 'stores' || $r_method == 'index' || $r_method == 'stock')) { ?> active <?php } ?>">
                    <a href="javascript:void(0)"><i class="fas fa-dolly-flatbed"></i> <span class="nav-label">Stores</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?php if($r_class == 'stores' && ($r_method == 'index')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>stores">Stores</a></li>
                        <li class="<?php if($r_class == 'stores' && ($r_method == 'stock')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>stores/stock">Stock</a></li>
                    </ul>
                </li>
            <?php } ?> 
            <?php if(get_session('admin_type') == '1' || get_session('admin_type') == '2' || get_session('admin_type') == '6') { ?> 
                <li class="<?php if($r_class == 'baking' && ($r_method == 'baking' || $r_method == 'index' || $r_method == 'production')) { ?> active <?php } ?>">
                    <a href="javascript:void(0)"><i class="fas fa-dolly-flatbed"></i> <span class="nav-label">Cooking/baking</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?php if($r_class == 'baking' && ($r_method == 'index')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>baking">Cooking/baking</a></li>
                        <li class="<?php if($r_class == 'baking' && ($r_method == 'production')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>baking/production">Production (Coming Soon)</a></li>
                    </ul>
                </li>
             <?php } ?>

        </ul>
    </div>
</nav>