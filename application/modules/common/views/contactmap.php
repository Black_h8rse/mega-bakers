<div style="display: none" id="contact-address"><?php echo get_setting('contact_detail','map_address') ?></div>
<div style="display: none" id="contact-lat"><?php echo get_setting('contact_detail','map_lat') ?></div>
<div style="display: none" id="contact-lng"><?php echo get_setting('contact_detail','map_lng') ?></div>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCvg5nJBPlbUREVRoK_JewTqTVXkqmurY&callback=initMap"  type="text/javascript"></script>
<script type="text/javascript">

    function initMap() {

        var address = $(document).find('#contact-address').html();
        var map_lat = $(document).find('#contact-lat').html();
        var map_lng = $(document).find('#contact-lng').html();
        var direct_address = encodeURIComponent(address);
        var broadway = {
            info: '<strong>'+address+'<br></strong>\
					<a target="blank" class="text-orange" href="https://www.google.com/maps/dir/?api=1&destination='+direct_address+'">Get Directions</a>',
            lat: map_lat,
            long: map_lng
        };
        var locations = [
            [broadway.info, broadway.lat, broadway.long, 0],
        ];
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: new google.maps.LatLng(map_lat, map_lng),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            var infowindow = new google.maps.InfoWindow();

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0], locations[i][6]);
                    infowindow.open(map, marker);
                }
            })(marker, i));

            infowindow.setContent(locations[i][0], locations[i][6]);
            infowindow.open(map, marker);
        }
    }

    // $(document).ready(function() {
    //     checkWidth();
    // });
    // function checkWidth() {
    //     if ($(window).width() < 767) {
    //
    //         $(window).scroll(function() {
    //             var scroll = $(window).scrollTop();
    //             if (scroll >= 5) {
    //
    //                 $('#top-bar').hide();
    //             }
    //             else
    //             {
    //                 $('#top-bar').show();
    //             }
    //         });
    //
    //     }
    //
    // }
</script>
