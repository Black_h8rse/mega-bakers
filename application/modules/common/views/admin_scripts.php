<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Toastr script -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/switchery/switchery.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/ckeditor/ckeditor.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/admin_assets/ckeditor/config.js"></script> -->


<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/pace/pace.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Sparkline -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/jquery.validate.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin_assets/js/jquery.maskedinput.min.js"></script>
<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>

