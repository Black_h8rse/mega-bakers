<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('index');
	}	
	public function upload_image()
	{
		echo "<pre>";
		print_r($_POST);
		print_r($_FILES);
		die();
	}
	public function find_match()
	{
        if($_POST){
            $data = $_POST;
            $data['vehicles'] = get_data('','vehicles',array('reg_no'=>$data['reg_no']));
            if( count( $data['vehicles'] ) > '0' ) {

            	$vehicle['vehicles'] 		= $data['vehicles'][0];
            	$vehicle['veh_id']      	= $data['vehicles'][0]['id'];
            	$vehicle['vehicle_meta']   	= get_data('','vehicle_meta',array('vehicle_id'=>$vehicle['veh_id']));
            	$vehicle['meta_count']     	= count($vehicle['vehicle_meta']);
            	$htmlrespon = $this->load->view('view_search_ajax' , $vehicle,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon);
                echo json_encode($finalResult);
                exit;

            } else {
            	$vehicle['meta_count']     = '0';
            	$htmlrespon = $this->load->view('view_search_ajax' , $vehicle,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon);
                echo json_encode($finalResult);
            }
        }else{
            show_admin404();
        }

	}
	public function upload_csv()
	{

		ini_set('max_execution_time', 300);
	 	$count	=	0;
        $fp = fopen(FCPATH . 'assets\csv.csv','r') or die("can't open file");
        while($csv_line = fgetcsv($fp,1024))
        {
            $count++;
            if($count == 1)
            {
                continue;
            }
            // for($i = 0, $j = count($csv_line); $i < $j; $i++)
            // {
    	            $data = array(
		                'restaurant_name' => $csv_line['0'] ,
		                'restaurant_state' => $csv_line['1'] ,
		                'restaurant_city' => $csv_line['2'] ,
		                'restaurant_postal_code' => $csv_line['3'] ,
		                'restaurant_street' => $csv_line['4'] ,
		                'restaurant_website' => $csv_line['5'] ,
		                'menu_name' => $csv_line['6'] ,
		                'menu_section_name' => $csv_line['7'] ,
		                'menu_section_description' => $csv_line['8'] ,
		                'menu_item_name' => $csv_line['9'] ,
		                'menu_item_description' => $csv_line['10'] ,
		                'menu_item_image_name' => $csv_line['11'] ,
		                'menu_item_image_path' => $csv_line['12'] ,
		                'price' => $csv_line['12']
		               );
		            $this->db->insert('menu_items', $data);
        	// }
         }
         fclose($fp) or die("can't close file");
	}
}
