<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
	public function get_stores()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('stores');
        return $this->db->get()->result_array();
    }
    public function insert_store($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' created a store '.$data['name']);
        $this->db->insert('log'); 

        $this->db->set('warehouse_id', $data['warehouse_id']);
        $this->db->set('phone', $data['phone']);
        $this->db->set('name', $data['name']);
        $this->db->set('size', "Unlimited");
        $this->db->set('status', '1');  
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->insert('stores');
        return $this->db->insert_id();
    }
    public function delete_store($id)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' deleted a store ');
        $this->db->insert('log');


        $this->db->where('id', $id);
        $this->db->delete('stores');
        return $this->db->affected_rows();
    }
    public function get_product_transfer()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('product_meta');
        return $this->db->get()->result_array();
    }
   
}

/* End of file Warehouse_model.php */
/* Location: ./application/modules/admin/models/Warehouse_model.php */