<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
	public function get_products()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('products');
        return $this->db->get()->result_array();
    }
    public function insert_product_type($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' created a new product type '.$data['name']);
        $this->db->insert('log');

        $this->db->set('table_name', 'products');
        $this->db->set('name', $data['name']);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->insert('types');
        return $this->db->insert_id();
    }   
    public function delete_product($id)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' deleted a product ');
        $this->db->insert('log');

        $this->db->where('id', $id);
        $this->db->delete('products');
        return $this->db->affected_rows();
    } 
    public function insert_product($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' created a new product '.$data['quantity']);
        $this->db->insert('log');

        
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('warehouse_id', $data['warehouse_id']);
        $this->db->set('store_id', $data['store_id']);
        $this->db->set('type_id', $data['product_id']);
        $this->db->set('product_weight', $data['product_weight']);
        $this->db->set('quantity', $data['quantity']);
        $this->db->set('paid_price', $data['price']);
        $this->db->set('price_per_piece', $data['out_put_price']);
        $this->db->set('status', '1');
        $this->db->insert('products');
        return $this->db->insert_id();
    }
   
}

/* End of file Warehouse_model.php */
/* Location: ./application/modules/admin/models/Warehouse_model.php */