<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
	public function get_users()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('users');
        return $this->db->get()->result_array();
    }
    public function delete_vehicle($id)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' deleted a vehicle ');
        $this->db->insert('log');


        $this->db->where('id', $id);
        $this->db->delete('vehicles');
        return $this->db->affected_rows();
    }
    public function delete_user($id)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' deleted a user ');
        $this->db->insert('log');



        $this->db->where('id', $id);
        $this->db->delete('users');
        return $this->db->affected_rows();






    }
    public function insert_user($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' add a new user '.$data['f_name'].' '.$data['l_name']);
        $this->db->insert('log');


        $this->db->set('f_name', $data['f_name']);
        $this->db->set('l_name', $data['l_name']);
        $this->db->set('phone', $data['phone']);
        $this->db->set('email', $data['email']);
        $this->db->set('password', hash( 'sha256', $data['password'] ) );
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('user_type', $data['user_type']);  
        $this->db->set('salary', $data['salary']);  
        $this->db->set('commission', $data['commission']);  
        $this->db->insert('users');
        return $this->db->insert_id();



        $this->db->set('f_name', $data['f_name']);
        $this->db->insert('users');
    }
    public function insert_vehicle($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' added a vehicle registration number '.$data['reg_no']);
        $this->db->insert('log');


        $this->db->set('name', $data['name']);
        $this->db->set('model', $data['model']);
        $this->db->set('year', $data['year']);
        $this->db->set('reg_no', $data['reg_no']);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('status', '1');  
        $this->db->insert('vehicles');
        return $this->db->insert_id();
    }
    public function get_user_details($data)
    {
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where('id',$data['user_id']);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function update_user($data)
    {
        $this->db->set('f_name', $data['f_name']);
        $this->db->set('l_name', $data['l_name']);
        $this->db->set('phone', $data['phone']);
        $this->db->where('id', $data['user_id']);
        $query = $this->db->update('users');
        return $this->db->affected_rows();
    }
}

/* End of file admin_model.php */
   /* Location: ./application/modules/admin/models/admin_model.php */