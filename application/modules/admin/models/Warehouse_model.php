<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
            //Do your magic here
    }
	public function get_warehouse()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('warehouses');
        return $this->db->get()->result_array();
    }
    public function get_stores()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('stores');
        return $this->db->get()->result_array();
    }

    public function get_warehouse_stock($warehouse_id,$product_type_id)
    {
        // $this->db->select("*");
        // $this->db->from('stores');
        // return $this->db->get()->result_array();

        $data = $this->db->select_sum('in_stock')
    ->from('products')
    ->where('warehouse_id', $warehouse_id)
    ->where('type_id', $product_type_id)
    ->where('store_id', '0')
    ->get();
return $data->row_array();


    }

    public function get_product_transfer()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('product_meta');
        return $this->db->get()->result_array();
    }
    public function get_warehouse_products()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('products');
        $this->db->where('product_type', '1');
        // $this->db->where('in_stock >', '0');
        return $this->db->get()->result_array();
    }
    public function get_warehouse_product_types()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('types');
        $this->db->where('table_name', 'warehouse_product');
        return $this->db->get()->result_array();
    }

    public function insert_warehouse($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' created a warehouse '.$data['name']);
        $this->db->insert('log');


        $this->db->set('name', $data['name']);
        $this->db->set('location', $data['location']);
        // $this->db->set('phone', $data['phone']); 
        $this->db->set('phone', '0000000000000'); 
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('type', '1');  
        $this->db->set('status', '1');  
        $this->db->insert('warehouses');
        return $this->db->insert_id();
    }
    public function delete_warehouse($id)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' deleted a warehouse ');
        $this->db->insert('log');

        $this->db->where('id', $id);
        $this->db->delete('warehouses');
        return $this->db->affected_rows();
    }
    public function delete_product($id)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' deleted a product from warehouse ');
        $this->db->insert('log');

        $this->db->where('id', $id);
        $this->db->delete('products');
        return $this->db->affected_rows();
    }
    public function delete_product_type($id)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' deleted a product type ');
        $this->db->insert('log');

        $this->db->where('id', $id);
        $this->db->delete('types');
        return $this->db->affected_rows();
    }
    public function insert_product_type($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' created a new product type for warehouse products '.$data['name']);
        $this->db->insert('log');

        $this->db->set('table_name', 'warehouse_product');
        $this->db->set('name', $data['name']);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->insert('types');
        return $this->db->insert_id();
    }
    public function insert_transfer_product($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' transfer product from warehouse ( '.get_name('warehouses','id',$data['w_h_id'],'name').' ) to store ( '.get_name('stores','id',$data['store_id'],'name').' )');
        $this->db->insert('log'); 

        $this->db->set('in_stock', $data['t_q_in_stock'] - $data['t_quantity']); 
        $this->db->where('id', $data['t_p_id']);
        $this->db->update('products');  

        $product_meta = get_data( '', 'product_meta', array( 'pro_id'=>$data['t_p_id'] ) ); 

        if( @$product_meta['0'] ) {
            $product_meta = $product_meta['0']; 
            $this->db->set('quantity', $product_meta['quantity'] + $data['t_quantity']); 
            $this->db->where('pro_id', $data['t_p_id']);
            $this->db->update('product_meta');
            return '1';   
        } else { 
            $this->db->set('pro_id', $data['t_p_id']);
            $this->db->set('pro_type_id ', $data['t_type_id']);
            $this->db->set('warehouse_id', $data['w_h_id']);
            $this->db->set('store_id', $data['store_id']);
            $this->db->set('quantity', $data['t_quantity']);
            $this->db->set('weight_type', $data['weight_type']);
            $this->db->set('detail', $data['t_detail']);
            $this->db->set('status ', '1');
            $this->db->set('created_by', get_session('admin_id'));
            $this->db->insert('product_meta'); 
            return $this->db->insert_id(); 
        }

    }
    public function insert_product($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' purchase a new product in warehouse '.$data['quantity']);
        $this->db->insert('log'); 

        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('warehouse_id', $data['warehouse_id']);
        $this->db->set('store_id', '0');
        $this->db->set('product_type', '1');
        $this->db->set('type_id', $data['product_id']);
        $this->db->set('weight_type', $data['product_weight']);
        $this->db->set('weight', $data['quantity']);
        $this->db->set('quantity', $data['quantity']);
        $this->db->set('in_stock', $data['quantity']);
        $this->db->set('paid_price', $data['total_price']);
        $this->db->set('price_per_piece', $data['price_per_piece']);
        $this->db->set('status', '1');
        $this->db->insert('products');
        return $this->db->insert_id();
    } 
   
}

/* End of file Warehouse_model.php */
/* Location: ./application/modules/admin/models/Warehouse_model.php */