<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
    public function get_users()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('users');
        return $this->db->get()->result_array();
    }
    public function check_old_password($data)
    {
        $this->db->select('*');
        $this->db->where('Password',hash('sha256', $data['old_password']));
        $this->db->where('id', get_session('admin_id'));
        $query = $this->db->get('users');
        return $query->num_rows();
    }

    public function change_admin_password($data)
    {     
        $this->db->set('password',hash('sha256', $data['new_password']));
        $this->db->where('id', get_session('admin_id'));
        $query = $this->db->update('users');
        return $this->db->affected_rows();   
    }
}

/* End of file admin_model.php */
   /* Location: ./application/modules/admin/models/admin_model.php */