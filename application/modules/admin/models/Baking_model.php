<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Baking_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
            //Do your magic here
    }
	public function get_warehouse()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('warehouses');
        return $this->db->get()->result_array();
    }
    public function insert_ingredient($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' created a new Ingredient '.$data['ingredient_name']); 
        $this->db->insert('log'); 
 
        $this->db->set('name', $data['ingredient_name']);
        $this->db->set('quantity', $data['quantity']);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->insert('ingredients');
        $ingredient_id = $this->db->insert_id(); 

        foreach ($data['meta'] as $value) {
            $this->db->set('ingredient_id', $ingredient_id);
            $this->db->set('pro_id', $value['key']); 
            $this->db->set('quantity', $value['value']); 
            $this->db->set('weight_type', $value['type']); 
            $this->db->set('created_by', get_session('admin_id'));  
            $this->db->insert('ingredients_meta');
        }

        return $this->db->insert_id();    
    }
    public function insert_production($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' created a new production ( '. get_name('types','id',$data['product_id'],'name'). ') From store ( '.get_name('stores','id',$data['store_id'],'name') . ' )'); 
        $this->db->insert('log'); 

        $product = get_data( '', 'products', array( 'type_id'=>$data['product_id'] ) );
        $product = $product[0];
        if( $product['ingredient_id'] > 0 ) { 
            $ingredients = get_data( '', 'ingredients_meta', array( 'ingredient_id'=>$product['ingredient_id'] ) );
        }

        foreach ($ingredients as $ingredient) {

        }
 
        $this->db->set('pro_id', $data['product_id']);
        $this->db->set('quantity', $data['quantity']);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('status', '1');
        $this->db->insert('production'); 
        return $this->db->insert_id();    
    }

    public function insert_product($data)
    {
        $this->db->set('url', 'Working on it');
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' created a new product type '.$data['name']);
        $this->db->set('msg', get_user_name( get_session('admin_id') ).' created a new product '.$data['name']);
        $this->db->insert('log'); 

        $this->db->set('table_name', 'baking_products');
        $this->db->set('name', $data['name']); 
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->insert('types');
        $type_id = $this->db->insert_id(); 


        $this->db->set('ingredient_id', $data['ingredient_id']);
        $this->db->set('type_id', $type_id); 
        $this->db->set('product_type', '2'); 
        $this->db->set('created_by', get_session('admin_id')); 
        $this->db->set('status', '1');  
        $this->db->insert('products');
        return $this->db->insert_id();
    }
    public function delete_product( $id, $type )
    {
        $this->db->where('id', $id);
        $this->db->delete('products');
        $id = $this->db->affected_rows();

        $this->db->where('id', $type);
        $this->db->delete('types');
        return $this->db->affected_rows();
    }
    public function delete_ingredient( $id )
    {
        $this->db->where('id', $id);
        $this->db->delete('ingredients');
        $id = $this->db->affected_rows();

        $this->db->where('ingredient_id', $id);
        $this->db->delete('ingredients_meta');
        return $this->db->affected_rows();
    }
   
}

/* End of file Warehouse_model.php */
/* Location: ./application/modules/admin/models/Warehouse_model.php */