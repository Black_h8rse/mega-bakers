<?php 
class Login_model extends CI_Model
{
	
	public function get_login($email,$password)
	{
		$this->db->select('*');
		$this->db->where('email',$email);		
		$this->db->where('password',hash('sha256', $password));
		// $this->db->where('password',$password);
		$query=$this->db->get('users');
		return $query->row();	
	}
	public function check_email($email)
	{
		$this->db->select('*');
		$this->db->where('Email',$email);
		$query = $this->db->get('admins');
		return $query->num_rows();
	}

	public function get_details($email)
	{
		$this->db->select('FirstName');
		$this->db->select('LastName');
		$this->db->select('Email');
		$this->db->where('Email',$email);			
		$query=$this->db->get('users');
		return $query->row();	
	}

	public function set_admin_password($email, $new_password)
	{
		$hash_pass="password('".$new_password."')";
		$this->db->set('Password',$hash_pass, FALSE);	
		$this->db->where('Email',$email);		
		$query=$this->db->update('admins');
		return $this->db->affected_rows();
	}		
}

?>