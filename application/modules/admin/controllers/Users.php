<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'user_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{
        $data['users'] = $this->user_model->get_users();
        $data['vehicles']   = get_data('','vehicles');
        $this->load->view('users/users', $data);
	}
    // 1=superadmin,2=workshop_admin,3=vehicle_owners,4=spare_part_shop_owners,5=mechanic  
    public function sparepart_shop_users()
    {
        $data['users'] = get_data('','users',array('user_type'=>'4','created_by'=>get_session('admin_id')));
        $this->load->view('users/sparepart_users', $data);
    }
    public function vehicle_users()
    {
        $data['users'] = get_data('','users',array('user_type'=>'3','created_by'=>get_session('admin_id')));
        $this->load->view('users/vehicle_users', $data);
    }
    public function workshop_users()
    {
        $data['users'] = get_data('','users',array('user_type'=>'2','created_by'=>get_session('admin_id')));
        $this->load->view('users/workshop_users', $data);
    }
    public function mechanic_users()
    {
        $data['users'] = get_data('','users',array('user_type'=>'5','created_by'=>get_session('admin_id')));
        $this->load->view('users/mechanic_users', $data);
    }
    public function delete_vehicle() {
        if ($_POST){
            $data = $this->input->post();
            $this->user_model->delete_vehicle($data['vehicle_id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.", 'id' => $data['vehicle_id']);
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
	public function delete_user() {
        if ($_POST){
            $data = $this->input->post();
            $slider_id = $this->user_model->delete_user($data['user_id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.", 'id' => $data['user_id']);
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function save_user()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('f_name', 'First Name', 'required');
            $this->form_validation->set_rules('l_name', 'Last Name', 'required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('user_type', 'User-Type', 'required');
            $this->form_validation->set_rules('salary', 'Salary', 'required');
            $this->form_validation->set_rules('commission', 'Commission', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Confirm password', 'required|matches[password]');

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $email_check = get_data('','users',array('email'=>$data['email']));
                if (empty($email_check)) {
                        $user_id = $this->user_model->insert_user($data);
                        $finalResult = array('msg' => 'success', 'response' => "User successfully inserted.", 'id' => $user_id);
                        echo json_encode($finalResult);
                        exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Email already exist');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
    public function save_vehicle()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('model', 'Model', 'required');
            $this->form_validation->set_rules('year', 'Year', 'required');
            $this->form_validation->set_rules('reg_no', 'Registration Number', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $email_check = get_data('','vehicles',array('reg_no'=>$data['reg_no']));
                if (empty($email_check)) {
                        $user_id = $this->user_model->insert_vehicle($data);
                        $finalResult = array('msg' => 'success', 'response' => "Vehicle successfully inserted.", 'id' => $user_id);
                        echo json_encode($finalResult);
                        exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Vehicle already exist');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
    public function edit_user()
    {
        if($_POST){
            $data = $_POST;
            $data['user'] = $this->user_model->get_user_details($data);
            if(!empty($data['user'])) {

                $htmlrespon = $this->load->view('users/edit_user_ajax' , $data,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon);
                echo json_encode($finalResult);
                exit;

            } else {
                $finalResult = array('msg' => 'error', 'response'=>"This record has been deleted from system!!!");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function update()
    {
        if($_POST){
            $data = $_POST;

            $this->form_validation->set_rules('f_name', 'First Name', 'required');
            $this->form_validation->set_rules('l_name', 'Last Name', 'required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'required');

            if ($this->form_validation->run($this) == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $user_status = $this->user_model->update_user($data);
                if($user_status > 0){
                    $finalResult = array('msg' => 'success', 'response'=>"User successfully updated.");
                    echo json_encode($finalResult);
                    exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Something went wrong!');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
}
