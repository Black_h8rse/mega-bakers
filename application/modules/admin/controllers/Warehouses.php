<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouses extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'warehouse_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{
        $data['warehouses'] = $this->warehouse_model->get_warehouse();
        $this->load->view('warehouses/warehouses', $data);
	}
    public function products()
    {
        $data['warehouse_products'] = $this->warehouse_model->get_warehouse_products();
        $data['warehouses']         = $this->warehouse_model->get_warehouse();
        $data['stores']             = $this->warehouse_model->get_stores();
        $data['product_types']      = $this->warehouse_model->get_warehouse_product_types();
        $data['in_stock']           = array();
        $i                          = 0;

        foreach ($data['warehouses'] as $warehouse) {
            foreach ($data['product_types'] as $product) {
                $data['in_stock'][$i]                   = $this->warehouse_model->get_warehouse_stock($warehouse['id'],$product['id']);  
                $data['in_stock'][$i]['warehouse_id']   = $warehouse['id'];
                $data['in_stock'][$i]['type_id']        = $product['id'];
                if(@get_name('products','warehouse_id',$warehouse['id'],'weight_type')){
                    $data['in_stock'][$i]['weight_type']    = get_name('products','warehouse_id',$warehouse['id'],'weight_type');
                } else {
                    $data['in_stock'][$i]['weight_type']    = 'null';
                }
                $data['in_stock'][$i]['id']             = $i;
                $i++;  
            }
            
        }


        $this->load->view('warehouses/warehouse_products',$data);
    }
    public function product_transfer()
    {
        $data['warehouse_products'] = $this->warehouse_model->get_warehouse_products();
        $data['warehouses']         = $this->warehouse_model->get_warehouse();
        $data['stores']             = $this->warehouse_model->get_stores();
        $data['product_meta']       = $this->warehouse_model->get_product_transfer();
        $data['product_types']      = $this->warehouse_model->get_warehouse_product_types();
        $this->load->view('warehouses/product_transfer',$data);
    }
    public function save_warehouse()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('name', 'Warehouse Name', 'required');
            $this->form_validation->set_rules('location', 'Location', 'required');
            // $this->form_validation->set_rules('phone', 'Phone Number', 'required'); 

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $email_check = get_data('','warehouses',array('name'=>$data['name']));
                if (empty($email_check)) {
                        $user_id = $this->warehouse_model->insert_warehouse($data);
                        $finalResult = array('msg' => 'success', 'response' => "Warehouse successfully inserted.", 'id' => $user_id);
                        echo json_encode($finalResult);
                        exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Name already exist');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
    public function save_transfer_product()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('store_id', 'Store', 'required');
            $this->form_validation->set_rules('t_q_in_stock', 'Quantity In Stock', 'required'); 
            $this->form_validation->set_rules('t_quantity', 'Total Quantity', 'required'); 
            $this->form_validation->set_rules('t_detail', 'Detail', 'required'); 

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            } else { 
                $id             = $this->warehouse_model->insert_transfer_product($data);  
                $finalResult    = array('msg' => 'success', 'response'=>'successfully Delivered');
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function delete_warehouse() {
        if ($_POST){
            $data = $this->input->post();
            $slider_id = $this->warehouse_model->delete_warehouse($data['id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.");
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
	public function delete_product() {
        if ($_POST){
            $data = $this->input->post();
            $slider_id = $this->warehouse_model->delete_product($data['id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.");
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function delete_product_type() {
        if ($_POST){
            $data = $this->input->post();
            $slider_id = $this->warehouse_model->delete_product_type($data['id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.");
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function save_product()
    {
        if ($_POST){
            $data = $this->input->post(); 
            $this->form_validation->set_rules('warehouse_id', 'Warehouse', 'required'); 
            $this->form_validation->set_rules('product_id', 'Product', 'required');
            $this->form_validation->set_rules('product_weight', 'Product Weight', 'required');
            $this->form_validation->set_rules('quantity', 'Quantity', 'required');
            $this->form_validation->set_rules('price_per_piece', 'Price Per Peice', 'required');
            $this->form_validation->set_rules('total_price', 'Total Price', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $user_id = $this->warehouse_model->insert_product($data);
                $finalResult = array('msg' => 'success', 'response' => "successfully inserted.");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function save_product_type()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('name', 'Name', 'required'); 
            if ($this->form_validation->run() == FALSE) {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $user_id = $this->warehouse_model->insert_product_type($data);
                $finalResult = array('msg' => 'success', 'response' => "successfully inserted.");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }

    public function check_equal_less($second_field,$first_field)
    {
        if ($second_field > $first_field) {
            $this->form_validation->set_message('check_equal_less', 'Please re-check quantity');
            return false;       
        }
        return true;
    }
   
}
