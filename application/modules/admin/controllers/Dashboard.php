<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'admin_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{
        $data['logs']	= get_data('','log');
		$this->load->view('dashboard', $data);
	}
}
