<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'products_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{
        $data['products'] 	= $this->products_model->get_products();
        $data['warehouses'] = get_data('','warehouses');
        $data['users'] 		= get_data('','users');
        $data['product_types'] = get_data('','types',array('table_name' => 'products'));
        $this->load->view('products/products', $data);
	}
	public function ajax_stores()
	{
		$data['stores'] 	= get_data('','stores',array('warehouse_id'=>$_POST['id']));
        $htmlrespon 		= $this->load->view('products/stores_dropdown_ajax' , $data,TRUE);
        $finalResult 		= array('msg' => 'success', 'response'=>$htmlrespon);
        echo json_encode($finalResult);
        exit;
	}
    public function delete_product() {
        if ($_POST){
            $data = $this->input->post();
            $slider_id = $this->products_model->delete_product($data['product_id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.");
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function save_product_type()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('name', 'Name', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
	            $user_id = $this->products_model->insert_product_type($data);
	            $finalResult = array('msg' => 'success', 'response' => "successfully inserted.");
	            echo json_encode($finalResult);
	            exit;
            }
        }else{
            show_admin404();
        }
    }
    public function save_product()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('warehouse_id', 'Warehouse', 'required');
            $this->form_validation->set_rules('store_id', 'Store', 'required');
            $this->form_validation->set_rules('product_id', 'Product', 'required');
            $this->form_validation->set_rules('product_weight', 'Product Weight', 'required');
            $this->form_validation->set_rules('quantity', 'Quantity', 'required');
            $this->form_validation->set_rules('price', 'Price', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $user_id = $this->products_model->insert_product($data);
                $finalResult = array('msg' => 'success', 'response' => "successfully inserted.");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
}
