<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stores extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'store_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{
        $data['stores'] = $this->store_model->get_stores();
        $data['warehouses'] = get_data('','warehouses');
        $data['users'] = get_data('','users');
        $this->load->view('stores/stores', $data);
	}
    public function stock()
    {
        $data['stores']         = $this->store_model->get_stores();
        $data['warehouses']     = get_data('','warehouses');
        $data['users']          = get_data('','users'); 
        $data['product_meta']   = $this->store_model->get_product_transfer(); 

        $this->load->view('stores/stock', $data);
    }
    public function save_store()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('warehouse_id', 'Warehouse', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required'); 
            $this->form_validation->set_rules('phone', 'Phone Number', 'required'); 

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $this->store_model->insert_store($data);
                $finalResult = array('msg' => 'success', 'response' => "Store successfully inserted.");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
	public function delete_store() {
        if ($_POST){
            $data           = $this->input->post();
            $slider_id      = $this->store_model->delete_store($data['id']);
            $finalResult    = array('msg' => 'success', 'response' => "successfully Deleted.");
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    
}
