<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Baking extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'baking_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{

        $data['products']       = get_data('', 'products', array('product_type'=>'2'), '*');
        $data['ingredients']    = get_data('', 'ingredients');
        $this->load->view('baking/baking', $data);
	} 
    public function production()
    { 

        $data['productions']    = get_data('', 'production'); 
        $data['products']       = get_data('', 'products', array('product_type'=>'2')); 
        $this->load->view('baking/production',$data);
    } 
    public function save_ingredient()
    {
        if ($_POST){
            $data = $this->input->post(); 
            $this->form_validation->set_rules('ingredient_name', 'Ingredient Name', 'required');
            $this->form_validation->set_rules('quantity', 'Quantity', 'required'); 

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $email_check = get_data('','ingredients',array('name'=>$data['ingredient_name']));
                if (empty($email_check)) {
                        $user_id = $this->baking_model->insert_ingredient($data);
                        $finalResult = array('msg' => 'success', 'response' => "Ingredient successfully inserted.", 'id' => $user_id);
                        echo json_encode($finalResult);
                        exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Name already exist');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
    public function save_production()
    {
        if ($_POST){
            $data = $this->input->post(); 
            $this->form_validation->set_rules('store_id', 'Store', 'required'); 
            $this->form_validation->set_rules('product_id', 'Product', 'required'); 
            $this->form_validation->set_rules('quantity', 'Quantity', 'required'); 

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{ 
                $production_id = $this->baking_model->insert_production($data);
                $finalResult = array('msg' => 'success', 'response' => "Product successfully inserted.", 'id' => $production_id);
                echo json_encode($finalResult);
                exit; 
            }
        }else{
            show_admin404();
        }
    }
    public function save_product()
    {
        if ($_POST){
            $data = $this->input->post(); 
            $this->form_validation->set_rules('name', 'Product Name', 'required');
            if ($this->form_validation->run() == FALSE) {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            } else {
                $name_check = get_data('','types',array('name'=>$data['name'],'table_name'=>'baking_products'));
                if (empty($name_check)) {
                        $user_id = $this->baking_model->insert_product($data);
                        $finalResult = array('msg' => 'success', 'response' => "Product successfully inserted.", 'id' => $user_id);
                        echo json_encode($finalResult);
                        exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Name already exist');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        } else {
            show_admin404();
        }
    }
	public function delete_product() {
        if ($_POST){
            $data           = $this->input->post();
            $this->baking_model->delete_product( $data['id'], $data['type'] );
            $finalResult    = array('msg' => 'success', 'response' => "successfully Deleted.");
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function delete_ingredient() {
        if ($_POST){
            $data           = $this->input->post();
            $this->baking_model->delete_ingredient( $data['id'] );
            $finalResult    = array('msg' => 'success', 'response' => "successfully Deleted.");
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
   
}
