<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Dashboard</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        <strong>Dashboard</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                        <div class="col-lg-3">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <span class="label label-success pull-right">Active</span>
                                    <h5>Users</h5>
                                </div>
                                <div class="ibox-content">
                                    <!-- <h1 class="no-margins"><?php echo get_count('users'); ?></h1> -->
                                    <div class="stat-percent font-bold text-success"><a href="<?php echo admin_url(); ?>user">View</a></div>
                                    <small>Total Users</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <span class="label label-info pull-right">Active</span>
                                    <h5>Vehicles</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="stat-percent font-bold text-success"><a href="<?php echo admin_url(); ?>vehicle">View</a></div>
                                    <small>Total Vehicles</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <span class="label label-primary pull-right">Active</span>
                                    <h5>Categories</h5>
                                </div>
                                <div class="ibox-content"> 
                                    <div class="stat-percent font-bold text-success"><a href="<?php echo admin_url(); ?>spare_parts">View</a></div>
                                    <small>Total Categories</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <span class="label label-warning pull-right">Active</span>
                                    <h5>Products</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="stat-percent font-bold text-success"><a href="<?php echo admin_url(); ?>spare_parts/sub_parts">View</a></div>
                                    <small>Total products</small>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <span class="label label-primary pull-right">Active</span>
                                    <h5>Categories</h5>
                                </div>
                                <div class="ibox-content"> 
                                    <div class="stat-percent font-bold text-success"><a href="<?php echo admin_url(); ?>spare_parts">View</a></div>
                                    <small>Total Categories</small>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <span class="label label-warning pull-right">Active</span>
                                    <h5>Products</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="stat-percent font-bold text-success"><a href="<?php echo admin_url(); ?>spare_parts/sub_parts">View</a></div>
                                    <small>Total products</small>
                                </div>
                            </div>
                        </div>
                    <div class="col-lg-3">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Active</span>
                                <h5>Invoices</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="stat-percent font-bold text-success"><a href="<?php echo admin_url(); ?>invoice">View</a></div>
                                <small>Total invoices</small>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Activity list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="activity-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <th>URL</th>
                                        <th>Message</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = '1'; foreach ($logs as $log) { ?>
                                            <tr>
                                                <td><?php echo $i;?></td>
                                                <td><a href="<?php echo $log['url'];?>"><?php echo $log['url'];?></a></td>
                                                <td><?php echo $log['msg'];?></td>
                                            </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>
    </div>

</div>
<?php $this->load->view('common/admin_scripts'); ?>
</body>
</html>

<script>
    $('#activity-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 0, 'desc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });

    $(document).on("click" , ".open_modal" , function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-val');

        $.ajax({
            url:url,
            type: 'POST',
            data: { id : id, old: 'old'},
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#activity_body').html(status.response);
                    $('#activity_modal').modal('show');
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });

    $(document).on("click" , ".admin-details" , function() {

        var admin = $(this).attr('data-val');

        $.ajax({
            url: admin_url+'users/edit_user',
            type: 'POST',
            data: { user_id : admin},
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#activity_body').html(status.response);
                    $('#activity_modal').modal('show');
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });

    $(document).on("click" , ".edit-section" , function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-val');

        $.ajax({
            url:url,
            type: 'POST',
            data: { id : id, old: ''},
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#activity_body').html(status.response);
                    $('#activity_modal').modal('show');
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });

    $(document).on("click" , ".log-records" , function(e) {
        var _this = $(this);
        var record = _this.attr('data-record');
        if (record=='none'){
            e.preventDefault();
            toastr.error('This record has been deleted from system!!!');
        }
    });

</script>