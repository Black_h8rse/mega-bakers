<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-8">
                <h2>Users</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Users</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-4" style="text-align: right;">
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_vehicle_modal">
                    <i class='fa fa-car'></i> Add Vehicles
                </button>
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_user_modal">
                    <i class='fa fa-plus'></i> Add User
                </button>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Users Activity list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="users-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if(get_session('admin_id') == '1'){?>
                                            <th>Created By</th>
                                        <?php } ?>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Type</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($users as $user){ ?>
                                        <tr id="<?php echo "row_".$i; ?>" class="<?php echo $user['id'];?>">
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $user['created_by'] != '0' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $user['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?>
                                            <td><?php echo $user['f_name']; ?></td>
                                            <td><?php echo $user['l_name']; ?></td>
                                            <td><?php echo $user['email']; ?></td>
                                            <td><?php echo $user['phone']; ?></td>
                                            <td>
                                                <?php if($user['user_type'] == 1) { ?>
                                                    <span class="label label-primary">Super Admin</span>
                                                <?php } elseif($user['user_type'] == 2) { ?>
                                                    <span class="label label-danger">Admin</span>
                                                <?php } elseif($user['user_type'] == 3) { ?>
                                                    <span class="label label-worning">Ware-House Manager</span>
                                                <?php } elseif($user['user_type'] == 4) { ?>
                                                    <span class="label label-success">Store Manager</span>
                                                <?php } elseif($user['user_type'] == 5) { ?>
                                                    <span class="label label-info">Production Manager</span>
                                                <?php } elseif($user['user_type'] == 6) { ?>
                                                    <span class="label label-warning">Driver</span>
                                                <?php } elseif($user['user_type'] == 7) { ?>
                                                    <span class="label label-info">Shef</span>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($user['created_at'])); ?></td>  
                                            <td>
                                                <?php if($user['id'] != '1'){?>

                                                    <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $user['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>

                                                    <!-- <button class="btn btn-primary btn-circle edit-btn" data-id="<?php echo $user['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button> -->
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Vehicles list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="vehicle-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if(get_session('admin_id') == '1'){?>
                                            <th>Created By</th>
                                        <?php } ?>
                                        <th>Name</th>
                                        <th>Model</th>
                                        <th>Year</th>
                                        <th>Registration no.</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($vehicles as $vehicle){ ?>
                                        <tr id="<?php echo "row_".$i; ?>" class="<?php echo "vehicle_".$vehicle['id'];?>">
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $vehicle['created_by'] != '0' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $vehicle['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?>
                                            <td><?php echo $vehicle['name']; ?></td>
                                            <td><?php echo $vehicle['model']; ?></td>
                                            <td><?php echo $vehicle['year']; ?></td>
                                            <td><?php echo $vehicle['reg_no']; ?></td>
                                            <td>
                                                <?php if($vehicle['status'] == 1) { ?>
                                                    <span class="label label-primary">Active</span>
                                                <?php } elseif($vehicle['status'] == 0) { ?>
                                                    <span class="label label-danger">InActive</span>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($user['created_at'])); ?></td>  
                                            <td>
                                                <button class="btn btn-danger btn-circle delete-vehicle-btn" data-id="<?php echo $vehicle['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_vehicle_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_vehicle_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New Vehicle</h5>
                    </div>
                    <form method="post" id="add_vehicle_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control" required="true">
                                </div>
                            </div>  
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Model</label>
                                    <input type="text" name="model" id="model" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Registration Year</label>
                                    <input type="number" name="year" id="year" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Registration No.</label>
                                    <input type="text" name="reg_no" id="reg_no" class="form-control" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_vehicle" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_user_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_user_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New User</h5>
                    </div>
                    <form method="post" id="add_user_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>First Name</label>
                                    <input type="text" name="f_name" id="f_name" class="form-control" required="true">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Last Name</label>
                                    <input type="text" name="l_name" id="l_name" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Email</label>
                                    <input type="text" name="email" id="email" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Phone Number</label>
                                    <input type="number" name="phone" id="phone" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Password</label>
                                    <input type="password" name="password" id="password" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Confirm Password</label>
                                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Salary</label>
                                    <input type="number" name="salary" id="salary" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Commission</label>
                                    <input type="number" name="commission" id="commission" class="form-control" required="true">
                                </div>
                            </div>

                            <?php if( get_session('admin_type') == '1' || get_session('admin_type') == '2') {  ?>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>User-Type</label>
                                    <select id="user_type" name="user_type" class="form-control">
                                        <option value="0">Select User-Type</option>
                                        <option value="1">Super Admin</option>
                                        <option value="2">Admin</option>
                                        <option value="3">Ware-House Manager</option>
                                        <option value="4">Store Manager</option>
                                        <option value="5">Production Manager</option>
                                        <option value="5">Drivers</option>
                                        <option value="6">Shefs</option>
                                    </select>
                                </div>
                            </div>
                            <?php } ?>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_user" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>
    </div>
</div>
<?php $this->load->view('common/admin_scripts'); ?>

</body>
</html>


<script type="text/javascript">
    $('#users-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 7, 'asc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });
    $('#vehicle-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 7, 'asc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    $(document).on('click', '.delete-vehicle-btn', function (event) {

        var vehicle_id = $(this).attr('data-id');
        
        swal({
                title: "Are you sure?",
                text: "You want to delete this vehicle!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'users/delete_vehicle',
                        type:'post',
                        data:{ vehicle_id : vehicle_id },
                        dataType:'json',
                        success:function(status){
                            toastr.success(status.response, 'Success');
                            $(".vehicle_"+vehicle_id).css("display","none");
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    $(document).on('click', '.delete-btn', function (event) {

        var user_id = $(this).attr('data-id');
        
        swal({
                title: "Are you sure?",
                text: "You want to delete this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'users/delete_user',
                        type:'post',
                        data:{ user_id : user_id },
                        dataType:'json',
                        success:function(status){
                            toastr.success(status.response, 'Success');
                            $("."+user_id).css("display","none");

                            // setTimeout(function () {
                            //     $(location).attr('href', admin_url+"user");
                            // }, 2000);
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , ".edit-btn" , function() {

        var user_id = $(this).attr('data-id');
        $.ajax({
            url:'<?php echo admin_url(); ?>user/edit_user',
            type: 'POST',
            data: { user_id : user_id},
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#add_user_body').html(status.response);
                    $('#add_user_modal').modal('show'); 
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_user" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_user_form").serialize();
        $.ajax({
            url:admin_url+'users/save_user',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_user_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_user_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_vehicle" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_vehicle_form").serialize();
        $.ajax({
            url:admin_url+'users/save_vehicle',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_vehicle_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_vehicle_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>



