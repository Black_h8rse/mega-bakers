<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-8">
                <h2>Products</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Products</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-4" style="text-align: right;">
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_product_type_modal">
                    <i class='fa fa-plus'></i>  Add Product Type
                </button>
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_product_modal">
                    <i class='fa fa-plus'></i>  Add Product
                </button>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Products list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="products-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <th style="display: none">id</th>
                                        <th>Warehouse Name</th>
                                        <th>Store Size</th>
                                        <th>Product Name</th>
                                        <th>Product Weight</th>
                                        <th>quantity</th>
                                        <!-- <th>paid_price</th>
                                        <th>price_per_piece</th> -->
                                        <th>Created Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($products as $product){ ?>
                                        <tr>
                                            <td class="product_<?php echo $product['id']; ?>"><?php echo $i; ?></td>
                                            <td style="display: none"><?php echo $product['id']; ?></td>
                                            <td><?php echo get_name('warehouses','id',$product['warehouse_id'],'name'); ?></td>
                                            <td><?php echo get_name('stores','id',$product['store_id'],'size'); ?></td>
                                            <td><?php echo get_name('types','id',$product['type_id'],'name'); ?></td>
                                            <td><?php echo $product['product_weight']; ?></td>   
                                            <td><?php echo $product['quantity']; ?></td>   
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($product['created_at'])); ?></td>
                                            <td>
                                                <?php if($product['status'] == 1) { ?>
                                                    <span class="label label-primary">Active</span>
                                                <?php } elseif($product['status'] == 0) { ?>
                                                    <span class="label label-danger">InActive</span>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <!-- <a href="<?php echo admin_url(); ?>products/edit/<?php echo $product['id']; ?>" class="btn btn-primary btn-circle" data-toggle="tooltip" title="Edit" data-placement="top"><i class="fa fa-eye"></i></a> -->
                                                <!-- <a href="<?php echo admin_url(); ?>products/edit/<?php echo $product['id']; ?>" class="btn btn-warning btn-circle" data-toggle="tooltip" title="Edit" data-placement="top"><i class="fa fa-paint-brush"></i></a> -->
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $product['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_product_type_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_product_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New Product Type</h5>
                    </div>
                    <form method="post" id="add_product_type_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control" required="true">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_product_type" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_product_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_product_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add Product</h5>
                    </div>
                    <form method="post" id="add_product_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Warehouses</label>
                                    <select id="warehouse_id" name="warehouse_id" class="form-control">
                                        <option>Select Warehouse</option>
                                        <?php foreach ($warehouses as $warehouse) { ?>
                                            <option value="<?php echo $warehouse['id'];?>"><?php echo $warehouse['name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row store_ajax_div" style="display: none;">
                                
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Products</label>
                                    <select id="product_id" name="product_id" class="form-control">
                                        <option>Select Product</option>
                                        <?php foreach ($product_types as $product) { ?>
                                            <option value="<?php echo $product['id'];?>"><?php echo $product['name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Product weight in</label>
                                    <select class="form-control" name="product_weight">
                                        <option >Select Product weight type</option>
                                        <option value="kg">KG</option>
                                        <option value="gram">Gram</option>
                                        <option value="liter">Liter</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Quantity</label>
                                    <input type="number" name="quantity" id="quantity" class="form-control input_price_or_quantity" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Total Price  <span class="price_per_piece" style="color: green;"></span> </label>
                                    <input type="number" name="price" id="price" class="form-control input_price_or_quantity" required="true">
                                    <input type="hidden" name="out_put_price" id="out_put_price">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_product" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>

    </div>

</div>
<?php $this->load->view('common/admin_scripts'); ?>

<!-- data tables  -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>



</body>
</html>

<script>
    $(document).ready(function() {
        $('#products-table').dataTable({
            "paging": true,
            "searching": true,
            "responsive": true,
            "order": [[ 7, 'asc' ]],
            "columnDefs": [
                { "responsivePriority": 1, "targets": 0 },
                { "responsivePriority": 2, "targets": -1 }
            ]
        });
    } );

    $(document).on('click', '.delete-btn', function (event) {

        var product_id = $(this).attr('data-id');

        swal({
                title: "Are you sure?",
                text: "You want to delete this Product!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'products/delete_product',
                        type:'post',
                        data:{ product_id : product_id },
                        dataType:'json',
                        success:function(status){

                            $(".product_"+product_id).css("display","none");
                            if(status.msg=='success'){
                                swal({title: "Success!", text: status.response, type: "success"},
                                    function(){
                                        location.reload();
                                    });

                            } else if(status.msg=='error'){

                                swal("Error", status.response, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , "#submit_product_type" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_product_type_form").serialize();
        $.ajax({
            url:admin_url+'products/save_product_type',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_product_type_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_product_type_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_product" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_product_form").serialize();
        $.ajax({
            url:admin_url+'products/save_product',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_product_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_product_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("change" , "#warehouse_id" , function() {
        var id = $(this).val();
        $.ajax({
            url:admin_url+'products/ajax_stores',
            type: 'POST',
            data: {id:id},
            dataType:'json',
            success:function(status){
                $(".store_ajax_div").html(status.response);
                $(".store_ajax_div").css("display","");
                
            }
        });
    });
    $(document).on("focusout" , ".input_price_or_quantity" , function() {
        var quantity        = $("#quantity").val();
        var price           = $("#price").val();
        if( price != '' && quantity != '' ){
            var out_put_price   = price/quantity;
            $(".price_per_piece").html("Price per piece("+out_put_price+")");
            $("#out_put_price").val(out_put_price);
        }
        
    });


</script>
