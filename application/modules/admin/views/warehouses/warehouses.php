<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-6">
                <h2>Warehouses</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Warehouses</strong>
                    </li>
                </ol>
            </div> 
            <div class="col-lg-6" style="text-align: right;">  
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_warehouse_modal">
                    <i class='fa fa-plus'></i>  Add Warehouse
                </button>
            </div>
        </div>
        

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Warehouses list</h5>
                           <!--  <a href="<?php echo admin_url(); ?>products/export_products" class="btn btn-primary pull-right" style="margin-top: -6px"><i class='fa fa-file-excel-o'></i> Export</a> -->
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="warehouse_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th> 
                                        <th>Name</th>
                                        <th>Location</th>
                                        <!-- <th>phone</th>  -->
                                        <th>Created Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($warehouses as $warehouse){ ?>
                                        <tr class="<?php echo $warehouse['id']; ?>">
                                            <td><?php echo $i; ?></td> 
                                            <td><?php echo $warehouse['name']; ?></td>
                                            <td><?php echo $warehouse['location']; ?></td>
                                            <!-- <td><?php // echo $warehouse['phone']; ?></td>  -->
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($warehouse['created_at'])); ?></td>
                                            <td>
                                                <?php if($warehouse['status'] == 1) { ?>
                                                    <span class="label label-primary">Active</span>
                                                <?php } elseif($warehouse['status'] == 0) { ?>
                                                    <span class="label label-danger">InActive</span>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <!-- <a href="<?php echo admin_url(); ?>products/edit/<?php echo $warehouse['id']; ?>" class="btn btn-warning btn-circle" data-toggle="tooltip" title="Edit" data-placement="top"><i class="fa fa-paint-brush"></i></a> -->
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $warehouse['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_warehouse_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_warehouse_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New Warehouse</h5>
                    </div>
                    <form method="post" id="add_warehouse_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Warehouse Name</label>
                                    <input type="text" name="name" id="name" class="form-control" required="true">
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Location</label>
                                    <input type="text" name="location" id="location" class="form-control" required="true">
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Phone Number</label>
                                    <input type="number" name="phone" id="phone" class="form-control" required="true">
                                </div>
                            </div> --> 
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_warehouse" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
        <?php $this->load->view('common/admin_footer'); ?>

    </div>

</div>
<?php $this->load->view('common/admin_scripts'); ?>

<script>
    $(document).ready(function() {
        $('#warehouse_table').dataTable({
            "paging": true,
            "searching": true,
            "responsive": true,
            "order": [[ 2, 'asc' ]],
            "columnDefs": [
                { "responsivePriority": 1, "targets": 0 },
                { "responsivePriority": 2, "targets": -1 }
            ]
        });
    } );

    $(document).on('click', '.delete-btn', function (event) {

        var id = $(this).attr('data-id');

        swal({
                title: "Are you sure?",
                text: "You want to delete this Product!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'warehouses/delete_warehouse',
                        type:'post',
                        data:{ id : id },
                        dataType:'json',
                        success:function(status){
                            if(status.msg=='success'){
                                $("."+id).css("display","none");
                                swal({title: "Success!", text: status.response, type: "success"},
                                    function(){
                                        location.reload();
                                    });
                            } else if(status.msg=='error'){
                                swal("Error", status.response, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , "#submit_warehouse" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_warehouse_form").serialize();
        $.ajax({
            url:admin_url+'warehouses/save_warehouse',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_warehouse_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_warehouse_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_product_type" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_product_type_form").serialize();
        $.ajax({
            url:admin_url+'products/save_product_type',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_product_type_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_product_type_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_product" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_product_form").serialize();
        $.ajax({
            url:admin_url+'products/save_product',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_product_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_product_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>


</body>
</html>