<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-6">
                <h2>Warehouse Products Transfer</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Warehouse Product Transfer</strong>
                    </li>
                </ol>
            </div>  
        </div>
        

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Warehouse Products Transfer list</h5> 
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="warehouse_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if( get_session('admin_id') == '1' ) {  ?> 
                                            <th>User Name</th>   
                                        <?php } ?> 
                                        <th>Warehouse Name</th> 
                                        <th>Store Name</th> 
                                        <th>Product Name</th> 
                                        <th>Total Quantity</th>  
                                        <th>Detail</th>  
                                        <th>Created Date</th> 
                                        <!-- <th>Action</th> -->
                                    </tr>
                                    </thead>
                                    <tbody> 
                                        <?php  $i  = 1; foreach ($product_meta as $product_t){  ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $product_t['created_by'] != '0' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $product_t['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?> 
                                            <td><?php echo get_name('warehouses','id',$product_t['warehouse_id'],'name'); ?></td>
                                            <td><?php echo  get_name('stores','id',$product_t['store_id'],'name'); ?></td>
                                            <td><?php echo  get_name('types','id',$product_t['pro_type_id'],'name'); ?></td> 
                                            <td><?php echo $product_t['quantity']; ?></td>
                                            <td><?php echo $product_t['detail']; ?></td> 
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($product_t['created_at'])); ?></td>
                                            <!-- <td>  
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $product['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td> -->
                                        </tr>
                                    <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        
        
        
        

        <?php $this->load->view('common/admin_footer'); ?>

    </div>

</div>
<?php $this->load->view('common/admin_scripts'); ?>

<script>
    $(document).ready(function() {
        $('#warehouse_table').dataTable({
            "paging": true,
            "searching": true,
            "responsive": true,
            "order": [[ 2, 'asc' ]],
            "columnDefs": [
                { "responsivePriority": 1, "targets": 0 },
                { "responsivePriority": 2, "targets": -1 }
            ]
        });
    } ); 


</script>


</body>
</html>