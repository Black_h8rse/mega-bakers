<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-6">
                <h2>Warehouse Products</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Warehouse Products</strong>
                    </li>
                </ol>
            </div> 
            <div class="col-lg-6" style="text-align: right;"> 
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_product_type_modal">
                    <i class='fa fa-plus'></i>  Add New Product
                </button>
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_product_modal">
                    <i class='fa fa-plus'></i>  Purchase
                </button> 
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Products Purchase list</h5> 
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="warehouse_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if( get_session('admin_id') == '1' ) {  ?> 
                                            <th>User Name</th>   
                                        <?php } ?> 
                                        <th>Warehouse Name</th> 
                                        <th>Product Name</th>
                                        <th>Weight Type</th>
                                        <th>Total Quantity</th>
                                        <th>Quantity Left</th>
                                        <th>Price Per Piece</th>
                                        <th>Total Amount</th>
                                        <th>Created Date</th> 
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody> 
                                        <?php  $i  = 1; foreach ($warehouse_products as $product){  ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $product['created_by'] != '0' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $product['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?> 
                                            <td><?php echo get_name('warehouses','id',$product['warehouse_id'],'name'); ?></td>
                                            <td><?php echo  get_name('types','id',$product['type_id'],'name'); ?></td>
                                            <td>
                                                <?php if($product['weight_type'] == 1) { ?>
                                                    <span class="label label-primary">Bags</span>
                                                <?php } elseif($product['weight_type'] == 2) { ?>
                                                    <span class="label label-danger">Box</span> 
                                                <?php } elseif($product['weight_type'] == 3) { ?>
                                                    <span class="label label-warning">Liter</span> 
                                                <?php } elseif($product['weight_type'] == 4) { ?>
                                                    <span class="label label-info">KG</span> 
                                                <?php } elseif($product['weight_type'] == 5) { ?>
                                                    <span class="label label-default">Gram</span>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $product['quantity']; ?></td>
                                            <td><?php echo $product['in_stock']; ?></td>
                                            <td><?php echo $product['price_per_piece']; ?></td>
                                            <td><?php echo $product['paid_price']; ?></td>
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($product['created_at'])); ?></td>
                                            <td> 

                                                <button class="btn btn-warning btn-circle transfer-pro-btn" data-id="<?php echo $product['id']; ?>" data-type-id="<?php echo $product['type_id']; ?>" data-name="<?php echo get_name('types','id',$product['type_id'],'name'); ?>" data-quantity="<?php echo $product['in_stock']; ?>" data-weight-type="<?php echo $product['weight_type']; ?>"   data-warehouse-id="<?php echo $product['warehouse_id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Tranfer"><i class="fa fa-bus"></i></button>
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $product['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                    <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Products in Stock</h5> 
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="product_in_stock_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <th>Warehouse Name</th> 
                                        <th>Product Name</th>
                                        <th>Weight Type</th>
                                        <th>Total Quantity</th>
                                    </tr>
                                    </thead>
                                    <tbody> 
                                        <?php  $i  = 1; foreach ($in_stock as $product){
                                            if($product['in_stock'] > '0'){ ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo get_name('warehouses','id',$product['warehouse_id'],'name'); ?></td>
                                                <td><?php echo  get_name('types','id',$product['type_id'],'name'); ?></td>
                                                <td>
                                                    <?php if($product['weight_type'] == 1) { ?>
                                                        <span class="label label-primary">Bags</span>
                                                    <?php } elseif($product['weight_type'] == 2) { ?>
                                                        <span class="label label-danger">Box</span> 
                                                    <?php } elseif($product['weight_type'] == 3) { ?>
                                                        <span class="label label-warning">Liter</span> 
                                                    <?php } elseif($product['weight_type'] == 4) { ?>
                                                        <span class="label label-info">KG</span> 
                                                    <?php } elseif($product['weight_type'] == 5) { ?>
                                                        <span class="label label-default">Gram</span>
                                                    <?php } ?>
                                                </td>
                                                <td><?php echo $product['in_stock']; ?></td>
                                            </tr>
                                        <?php } $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Products Type list</h5>
                           <!--  <a href="<?php echo admin_url(); ?>products/export_products" class="btn btn-primary pull-right" style="margin-top: -6px"><i class='fa fa-file-excel-o'></i> Export</a> -->
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="product_type_list_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if( get_session('admin_id') == '1' ) {  ?> 
                                            <th>User Name</th>   
                                        <?php } ?> 
                                        <th>Name</th>  
                                        <th>Created Date</th> 
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody> 
                                    <?php $i=1; foreach ($product_types as $type){ ?>
                                        <tr class="product_type_<?php echo $type['id'];?>">
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $type['created_by'] != '0' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $type['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?> 
                                            <td><?php echo $type['name']; ?></td>
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($type['created_at'])); ?></td>
                                            <td>
                                                <button class="btn btn-danger btn-circle delete-type-btn" data-id="<?php echo $type['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="transfer_product_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_product_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Transfer Product</h5>
                    </div>
                    <form method="post" id="transfer_product_form"> 
                        <input type="hidden" name="t_p_id" id="t_p_id" value="">
                        <input type="hidden" name="w_h_id" id="w_h_id" value=""> 
                        <input type="hidden" name="t_type_id" id="t_type_id" value=""> 
                        <input type="hidden" name="weight_type" id="weight_type" value="">
                        <div class="modal-body">

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Stores</label>
                                    <select id="store_id" name="store_id" class="form-control">
                                        <option value="">Please select store</option> 
                                        <?php foreach ($stores as $store) { ?>
                                            <option value="<?php echo $store['id'];?>"><?php echo $store['name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="t_name" id="t_name" value="" readonly>
                                </div>
                            </div>

                            

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Quantity In Stock</label>
                                    <input type="text" class="form-control" name="t_q_in_stock" id="t_q_in_stock" value="" readonly>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Transfer Quantity</label>
                                    <input type="number" class="form-control" name="t_quantity" id="t_quantity" value="">
                                </div>
                            </div> 

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Detail</label>
                                    <input type="text" class="form-control" name="t_detail" id="t_detail" value="">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_transfer_product" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_product_type_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_product_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New Product</h5>
                    </div>
                    <form method="post" id="add_product_type_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control" required="true">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_product_type" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_product_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_product_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Purchase</h5>
                    </div>
                    <form method="post" id="add_product_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Warehouses</label>
                                    <select id="warehouse_id" name="warehouse_id" class="form-control">
                                        <option>Select Warehouse</option>
                                        <?php foreach ($warehouses as $warehouse) { ?>
                                            <option value="<?php echo $warehouse['id'];?>"><?php echo $warehouse['name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Product</label>
                                    <select id="product_id" name="product_id" class="form-control">
                                        <option>Select Product</option>
                                        <?php foreach ($product_types as $product) { ?>
                                            <option value="<?php echo $product['id'];?>"><?php echo $product['name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Product weight in</label>
                                    <select class="form-control" name="product_weight">
                                        <option >Select Product weight type</option>
                                        <option value="1">Bag</option>
                                        <option value="2">Box</option> 
                                        <option value="3">Liter</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Quantity</label>
                                    <input type="number" name="quantity" id="quantity" class="form-control input_price_or_quantity" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Price Per piece: </label>
                                    <input type="number" name="price_per_piece" id="price_per_piece" class="form-control input_price_or_quantity" required="true"> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Total Amount: </label>
                                    <input type="number" name="total_price" id="total_price" class="form-control total_price_input" readonly> 
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_product" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>

    </div>

</div>
<?php $this->load->view('common/admin_scripts'); ?>

<script>
    $(document).ready(function() {
        $('#warehouse_table').dataTable({
            "paging": true,
            "searching": true,
            "responsive": true,
            "order": [[ 1, 'asc' ]],
            "columnDefs": [
                { "responsivePriority": 1, "targets": 0 },
                { "responsivePriority": 2, "targets": -1 }
            ]
        });
        $('#product_in_stock_table').dataTable({
            "paging": true,
            "searching": true,
            "responsive": true,
            "order": [[ 1, 'asc' ]],
            "columnDefs": [
                { "responsivePriority": 1, "targets": 0 },
                { "responsivePriority": 2, "targets": -1 }
            ]
        });
        $('#product_type_list_table').dataTable({
            "paging": true,
            "searching": true,
            "responsive": true,
            "order": [[ 1, 'asc' ]],
            "columnDefs": [
                { "responsivePriority": 1, "targets": 0 },
                { "responsivePriority": 2, "targets": -1 }
            ]
        });
    } );

    $(document).on('blur', '.input_price_or_quantity', function (event) {
        var quantity            = $("#quantity").val();
        var price_per_piece     = $("#price_per_piece").val();
        var total_price         = quantity*price_per_piece;
        $("#total_price").val(total_price); 
    });
    $(document).on('click', '.delete-type-btn', function (event) {

        var id = $(this).attr('data-id');

        swal({
                title: "Are you sure?",
                text: "You want to delete this Product-Type!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'warehouses/delete_product_type',
                        type:'post',
                        data:{ id : id },
                        dataType:'json',
                        success:function(status){
                            if(status.msg=='success'){
                                $(".product_type_"+id).css("display","none");
                                swal({title: "Success!", text: status.response, type: "success"},
                                    function(){
                                        location.reload();
                                    });
                            } else if(status.msg=='error'){
                                swal("Error", status.response, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    $(document).on('click', '.delete-btn', function (event) {

        var id = $(this).attr('data-id');

        swal({
                title: "Are you sure?",
                text: "You want to delete this Product!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'warehouses/delete_product',
                        type:'post',
                        data:{ id : id },
                        dataType:'json',
                        success:function(status){
                            if(status.msg=='success'){
                                $("."+id).css("display","none");
                                swal({title: "Success!", text: status.response, type: "success"},
                                    function(){
                                        location.reload();
                                    });
                            } else if(status.msg=='error'){
                                swal("Error", status.response, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , ".transfer-pro-btn" , function() { 
        $('#transfer_product_modal').modal('show');
        $('#t_p_id').val($(this).attr("data-id"));
        $('#t_name').val($(this).attr("data-name"));
        $('#t_q_in_stock').val($(this).attr("data-quantity"));
        $('#weight_type').val($(this).attr("data-weight-type")); 
        $('#w_h_id').val($(this).attr("data-warehouse-id")); 
        $('#t_type_id').val($(this).attr("data-type-id")); 

    });
    $(document).on("click" , "#submit_warehouse" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_warehouse_form").serialize();
        $.ajax({
            url:admin_url+'warehouses/save_warehouse',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_warehouse_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_warehouse_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_transfer_product" , function() {
        if( parseInt($("#t_quantity").val()) > parseInt($("#t_q_in_stock").val()) ) { 
            toastr.error("Please re-check quantity"); 
        } else { 
            var btn = $(this).ladda();
            btn.ladda('start');
            var formData = $("#transfer_product_form").serialize();
            $.ajax({
                url:admin_url+'warehouses/save_transfer_product',
                type: 'POST',
                data: formData,
                dataType:'json',
                success:function(response){
                    console.log(response);
                    btn.ladda('stop');
                    if(response.msg=='success') {
                        $('#transfer_product_form')[0].reset();
                        toastr.success(response.response);
                        $('#transfer_product_form').modal('hide');
                        setTimeout(function(){ location.reload(); }, 2000);
                    } else if(response.msg == 'error') {
                        toastr.error(response.response);
                    }
                }
            });
        }
    });
    $(document).on("click" , "#submit_product_type" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_product_type_form").serialize();
        $.ajax({
            url:admin_url+'warehouses/save_product_type',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_product_type_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_product_type_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_product" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_product_form").serialize();
        $.ajax({
            url:admin_url+'warehouses/save_product',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_product_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_product_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>


</body>
</html>