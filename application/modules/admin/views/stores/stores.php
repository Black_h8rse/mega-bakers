<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-6">
                <h2>Stores</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Stores</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-6" style="text-align: right;">  
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_store_modal">
                    <i class='fa fa-plus'></i>  Add Store
                </button>
            </div> 
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Store list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                

                                <table id="users-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if(get_session('admin_id') == '1'){?>
                                            <th>Created By</th>
                                        <?php } ?>
                                        <!-- <th style="display: none">id</th> -->
                                        <th>Warehouse Name</th>
                                        <th>Store Name</th>
                                        <!-- <th>Phone</th> -->
                                        <!-- <th>Size</th> -->
                                        <th>Created Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($stores as $store){ ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $store['created_by'] != '0' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $store['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?>
                                            <!-- <td style="display: none"><?php echo $store['id']; ?></td> -->
                                            <td><?php echo get_name('warehouses','id',$store['warehouse_id'],'name'); ?></td> 
                                            <td><?php echo $store['name']; ?></td>
                                            <!-- <td><?php echo $store['phone']; ?></td> -->
                                            <!-- <td><?php echo $store['size']; ?></td> -->
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($store['created_at'])); ?></td>
                                            <td>
                                                <?php if($store['status'] == 1) { ?>
                                                    <span class="label label-primary">Active</span>
                                                <?php } elseif($store['status'] == 0) { ?>
                                                    <span class="label label-danger">InActive</span>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $store['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_store_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_warehouse_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New Store</h5>
                    </div>
                    <form method="post" id="add_store_form">
                        <div class="modal-body">

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Warehouses</label>
                                    <select id="warehouse_id" name="warehouse_id" class="form-control">
                                        <option value="0">Select Warehouse</option>
                                        <?php foreach ($warehouses as $warehouse) { ?>
                                            <option value="<?php echo $warehouse['id'];?>"><?php echo $warehouse['name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control" required="true">
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Phone Number</label>
                                    <input type="number" name="phone" id="phone" class="form-control" required="true">
                                </div>
                            </div> 

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_store" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>

    </div>

</div>
<?php $this->load->view('common/admin_scripts'); ?>

</body>
</html>

<script>
    $(document).ready(function() {
        $('#users-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 7, 'asc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });
    } );

    $(document).on('click', '.delete-btn', function (event) {

        var id = $(this).attr('data-id');

        swal({
                title: "Are you sure?",
                text: "You want to delete this Store!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'stores/delete_store',
                        type:'post',
                        data:{ id : id },
                        dataType:'json',
                        success:function(status){

                            if(status.msg=='success'){
                                swal({title: "Success!", text: status.response, type: "success"},
                                    function(){
                                        location.reload();
                                    });
                            } else if(status.msg=='error'){

                                swal("Error", status.response, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , "#submit_store" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_store_form").serialize();
        $.ajax({
            url:admin_url+'stores/save_store',
            type: 'POST',
            data: formData,
            dataType:'JSON',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_store_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_store_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>
