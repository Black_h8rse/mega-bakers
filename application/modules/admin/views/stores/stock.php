<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-6">
                <h2>Stock</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Stock</strong>
                    </li>
                </ol>
            </div> 
        </div>

        <div class="wrapper wrapper-content animated fadeInRight"> 
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Stock List</h5> 
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="warehouse_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if( get_session('admin_id') == '1' ) {  ?> 
                                            <th>User Name</th>   
                                        <?php } ?> 
                                        <th>Warehouse Name</th> 
                                        <th>Store Name</th> 
                                        <th>Product Name</th> 
                                        <th>Weight Type</th> 
                                        <th>Total Quantity</th>  
                                        <th>Detail</th>  
                                        <th>Created Date</th> 
                                        <!-- <th>Action</th> -->
                                    </tr>
                                    </thead>
                                    <tbody> 
                                        <?php  $i  = 1; foreach ($product_meta as $product_t){  ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $product_t['created_by'] != '0' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $product_t['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?> 
                                            <td><?php echo get_name('warehouses','id',$product_t['warehouse_id'],'name'); ?></td>
                                            <td><?php echo  get_name('stores','id',$product_t['store_id'],'name'); ?></td>
                                            <td><?php echo  get_name('types','id',$product_t['pro_type_id'],'name'); ?></td> 
                                            <td>
                                                <?php if($product_t['weight_type'] == 1) { ?>
                                                    <span class="label label-primary">Bags</span>
                                                <?php } elseif($product_t['weight_type'] == 2) { ?>
                                                    <span class="label label-danger">Box</span> 
                                                <?php } elseif($product_t['weight_type'] == 3) { ?>
                                                    <span class="label label-warning">Liter</span> 
                                                <?php } elseif($product_t['weight_type'] == 4) { ?>
                                                    <span class="label label-info">KG</span> 
                                                <?php } elseif($product_t['weight_type'] == 5) { ?>
                                                    <span class="label label-default">Gram</span>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $product_t['quantity']; ?></td>
                                            <td><?php echo $product_t['detail']; ?></td> 
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($product_t['created_at'])); ?></td>
                                            <!-- <td>  
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $product['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td> -->
                                        </tr>
                                    <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <?php $this->load->view('common/admin_footer'); ?>

    </div>

</div>
<?php $this->load->view('common/admin_scripts'); ?>

</body>
</html>

<script>
    $(document).ready(function() {
        $('#warehouse_table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 7, 'asc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });
    } );

    $(document).on('click', '.delete-btn', function (event) {

        var id = $(this).attr('data-id');

        swal({
                title: "Are you sure?",
                text: "You want to delete this Product!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'stores/delete_store',
                        type:'post',
                        data:{ id : id },
                        dataType:'json',
                        success:function(status){

                            if(status.msg=='success'){
                                swal({title: "Success!", text: status.response, type: "success"},
                                    function(){
                                        location.reload();
                                    });
                            } else if(status.msg=='error'){

                                swal("Error", status.response, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    }); 

</script>
