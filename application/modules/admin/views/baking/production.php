<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-6">
                <h2>Production</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Production</strong>
                    </li>
                </ol>
            </div> 
            <div class="col-lg-6" style="text-align: right;"> 
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_production_module">
                    <i class='fa fa-plus'></i>  Add Production
                </button>
            </div>
        </div>
         
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Production list</h5> 
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="production_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Sr#</th> 
                                            <th>Name</th> 
                                            <th>Quantity</th> 
                                            <th>Date</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($productions as $production){ ?>
                                        <tr class="<?php echo "production".$production['id']; ?>">
                                            <td><?php echo $i; ?></td> 
                                            <td><?php echo get_name('types','id', $production['pro_id'], 'name') ?></td>  
                                            <td><?php echo $production['quantity']; ?></td>  
                                            <td><?php echo date('F jS, Y - h:i a',strtotime($production['created_at'])); ?></td>
                                            <td> 
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $production['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Product In Stock list</h5> 
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="production_in_stock_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Sr#</th> 
                                            <th>Name</th> 
                                            <th>Quantity</th> 
                                            <th>Date</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($productions as $production){ ?>
                                        <tr class="<?php echo $production['id']; ?>">
                                            <td><?php echo $i; ?></td> 
                                            <td><?php echo get_name('types','id', $production['pro_id'], 'name') ?></td>  
                                            <td><?php echo $production['quantity']; ?></td>   
                                            <td><?php echo date('F jS, Y - h:i a',strtotime($production['created_at'])); ?></td>
                                            <td> 
                                                <button class="btn btn-danger btn-circle delete-production-btn" data-id="<?php echo $production['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_production_module" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_warehouse_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add Product</h5>
                    </div>
                    <form method="post" id="add_production_form">
                        <div class="modal-body"> 
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Store</label>
                                    <select class="form-control" id="store_id" name="store_id">
                                        <option value="0">Please select store</option>
                                        <?php foreach ( get_data('','stores') as $store) { ?> 
                                            <option value="<?php echo $store['id'];?>"><?php echo $store['name'];?></option> 
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Product</label>
                                    <select class="form-control" id="product_id" name="product_id">
                                        <option value="0">Please select Product</option>
                                        <?php foreach ( get_data('','types',array('table_name'=>'baking_products')) as $product) { ?> 
                                            <option value="<?php echo $product['id'];?>"><?php echo $product['name'];?></option> 
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Quantity</label>
                                    <input type="text" name="quantity" id="quantity" class="form-control" required="true">
                                </div>
                            </div>  
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" class="ladda-button btn btn-primary" id="submit_product" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>

    </div>

</div>
<?php $this->load->view('common/admin_scripts'); ?>

<script>
    $(document).ready(function() {
        $('#production_table').dataTable({
            "paging": true,
            "searching": true,
            "responsive": true,
            "order": [[ 2, 'asc' ]],
            "columnDefs": [
                { "responsivePriority": 1, "targets": 0 },
                { "responsivePriority": 2, "targets": -1 }
            ]
        });   
        $('#production_in_stock_table').dataTable({
            "paging": true,
            "searching": true,
            "responsive": true,
            "order": [[ 2, 'asc' ]],
            "columnDefs": [
                { "responsivePriority": 1, "targets": 0 },
                { "responsivePriority": 2, "targets": -1 }
            ]
        });   

    } );
    $(document).on('click', '.delete-production-btn', function (event) {

        var id = $(this).attr('data-id'); 

        swal({
                title: "Are you sure?",
                text: "You want to delete this Production!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'baking/delete_production',
                        type:'post',
                        data:{ id : id},
                        dataType:'json',
                        success:function(status){
                            if(status.msg=='success'){
                                $(".production"+id).css("display","none");
                                swal({title: "Success!", text: status.response, type: "success"},
                                    function(){
                                        location.reload();
                                    });
                            } else if(status.msg=='error'){
                                swal("Error", status.response, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("submit" , "#add_production_form" , function(e) {
        e.preventDefault(); 
        var formData = $("#add_production_form").serialize();
        $.ajax({
            url:admin_url+'baking/save_production',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){ 
                if(status.msg=='success') {
                    $('#add_production_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_production_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>


</body>
</html>