<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-6">
                <h2>Baking</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Baking</strong>
                    </li>
                </ol>
            </div> 
            <div class="col-lg-6" style="text-align: right;"> 
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_baking_product">
                    <i class='fa fa-plus'></i>  Add Product
                </button>
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_ingredient_modal">
                    <i class='fa fa-plus'></i>  Add Ingredients
                </button> 
            </div>
        </div>
         
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Product list</h5> 
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="products_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Sr#</th> 
                                            <th>Product Name</th>
                                            <th>Ingredient Name</th> 
                                            <th>Created Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($products as $product){ ?>
                                        <tr class="<?php echo $product['id']; ?>">
                                            <td><?php echo $i; ?></td> 
                                            <td><?php echo get_name('types','id', $product['type_id'], 'name') ?></td>
                                            <td><?php if($product['ingredient_id']) {  echo get_name('ingredients','id', $product['ingredient_id'], 'name'); } else { echo "-----"; }?></td>  
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($product['created_at'])); ?></td>
                                            <td>
                                                <?php if($product['status'] == 1) { ?>
                                                    <span class="label label-primary">Active</span>
                                                <?php } elseif($product['status'] == 0) { ?>
                                                    <span class="label label-danger">InActive</span>
                                                <?php } ?>
                                            </td>
                                            <td> 
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $product['id']; ?>"  data-type="<?php echo $product['type_id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Ingredient list</h5> 
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="ingredient_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Sr#</th>  
                                            <th>Ingredient Name</th> 
                                            <th>Quantity</th> 
                                            <th>Created Date</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($ingredients as $ingredient){ ?>
                                        <tr class="<?php echo "ingredient".$ingredient['id']; ?>">
                                            <td><?php echo $i; ?></td>  
                                            <td><?php if($ingredient['id']) {  echo get_name('ingredients','id', $ingredient['id'], 'name'); } else { echo "-----"; }?></td>  
                                            <td><?php echo $ingredient['quantity']; ?></td>  
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($ingredient['created_at'])); ?></td> 
                                            <td> 
                                                <button class="btn btn-danger btn-circle delete-ingredient-btn" data-id="<?php echo $ingredient['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_ingredient_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_ingredient_body" style="max-width: 1000px !important;">
              <div class="modal-content animated flipInY">
                <div class="modal-header">
                  <h5 style="float: left;" class="modal-title">Add Ingredients</h5> 
                  <button style="float: right;" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <button style="float: right; margin-right: 18px;" class="btn btn-primary btn-sm pull-right mb-3 add-option">Add Option <i class="fa fa-plus"></i></button>
                </div>
                <form method="post" id="submit_ingredient_form">
                  <div class="modal-body">
                    <div class="box-body">
                      <div class="col-md-12"> 
                        <div class="col-md-2"> 
                          <label>Ingredient Name:</label>
                        </div>
                        <div class="col-md-12"> 
                          <input type="text" class="form-control" id="ingredient_name" name="ingredient_name" value="" required="">
                        </div>
                      </div> 
                      <hr>
                    </div>
                    <div class="box-body template-option">
                      <div class="col-md-12"> 
                        <div class="col-md-2"> 
                          <label>Quantity in Units:</label>
                        </div>
                        <div class="col-md-12"> 
                          <input type="text" class="form-control" id="quantity" name="quantity" value="" required="">
                        </div>
                      </div> 
                      <hr>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="action" value="add_ingredient">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="ladda-button btn btn-primary" id="submit_ingredient" data-style="expand-right">Add</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

        <div class="modal inmodal" id="add_baking_product" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_warehouse_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add Product</h5>
                    </div>
                    <form method="post" id="add_product_form">
                        <div class="modal-body"> 
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control" required="true">
                                </div>
                            </div> 
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Ingredient (<span style="color:gray"> Optional </span>) </label>
                                    <select class="form-control" id="ingredient_id" name="ingredient_id">
                                        <option value="0">Please select ingredient if required</option>
                                        <?php foreach ($ingredients as $ingredient) { ?> 
                                            <option value="<?php echo $ingredient['id'];?>"><?php echo $ingredient['name'];?></option> 
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>  
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" class="ladda-button btn btn-primary" id="submit_product" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>

    </div>

</div>
<?php $this->load->view('common/admin_scripts'); ?>

<script>
    $(document).ready(function() {
        $('#products_table').dataTable({
            "paging": true,
            "searching": true,
            "responsive": true,
            "order": [[ 2, 'asc' ]],
            "columnDefs": [
                { "responsivePriority": 1, "targets": 0 },
                { "responsivePriority": 2, "targets": -1 }
            ]
        });        
        $('#ingredient_table').dataTable({
            "paging": true,
            "searching": true,
            "responsive": true,
            "order": [[ 2, 'asc' ]],
            "columnDefs": [
                { "responsivePriority": 1, "targets": 0 },
                { "responsivePriority": 2, "targets": -1 }
            ]
        });
    } );
    var counter = '0';
    $('.add-option').on('click', function( e ) {
        e.preventDefault();
        var new_option = '<div class="ibox-content option-row-'+counter+'">\n'+
        '<div class="form-group row">\n'+
            '<label class="col-sm-1 col-form-label" style="padding-top: 5px;">Name:</label>\n'+
            '<div class="col-sm-3">\n'+
            '<select class="form-control" name="meta['+counter+'][key]" required="true">\n'+ 
              '<option value="">Please select ingredient</option>\n'+
              <?php foreach ( get_data('','types') as $value ) { ?>
                '<option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>\n'+
              <?php } ?>
            '</select>\n'+
            '</div>\n'+
            '<label class="col-sm-1 col-form-label" style="padding-top: 5px;">Quantity:</label>\n'+
            '<div class="col-sm-2">\n'+
                '<input type="number" class="form-control" name="meta['+counter+'][value]" required="true">\n'+
            '</div>\n'+
            '<label class="col-sm-1 col-form-label" style="padding-top: 5px;">Type:</label>\n'+
            '<div class="col-sm-3">\n'+
              '<select class="form-control" name="meta['+counter+'][type]" required="true">\n'+ 
                '<option value="">Please select Type</option>\n'+ 
                  '<option value="gram">Gram</option>\n'+ 
                  '<option value="kilo">KG</option>\n'+ 
                  '<option value="liter">Liter</option>\n'+  
              '</select>\n'+
            '</div>\n'+
            '<button class="btn btn-danger btn-sm mb-1 remove-option" data-val="'+counter+'"><i class="fa fa-times"></i></button>\n'+
        '</div>\n';
        $('.template-option').append(new_option); 
        counter++; 
    });
    $(document).on('click', '.delete-ingredient-btn', function (event) {

        var id = $(this).attr('data-id'); 

        swal({
                title: "Are you sure?",
                text: "You want to delete this ingredient!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'baking/delete_ingredient',
                        type:'post',
                        data:{ id : id},
                        dataType:'json',
                        success:function(status){
                            if(status.msg=='success'){
                                $(".ingredient"+id).css("display","none");
                                swal({title: "Success!", text: status.response, type: "success"},
                                    function(){
                                        location.reload();
                                    });
                            } else if(status.msg=='error'){
                                swal("Error", status.response, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    $(document).on('click', '.delete-btn', function (event) {

        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');

        swal({
                title: "Are you sure?",
                text: "You want to delete this Product!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'baking/delete_product',
                        type:'post',
                        data:{ id : id, type : type },
                        dataType:'json',
                        success:function(status){
                            if(status.msg=='success'){
                                $("."+id).css("display","none");
                                swal({title: "Success!", text: status.response, type: "success"},
                                    function(){
                                        location.reload();
                                    });
                            } else if(status.msg=='error'){
                                swal("Error", status.response, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("submit" , "#submit_ingredient_form" , function(e) {
        e.preventDefault(); 
        var formData = $("#submit_ingredient_form").serialize();
        $.ajax({
            url:admin_url+'baking/save_ingredient',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){ 
                if(status.msg=='success') {
                    $('#submit_ingredient_form')[0].reset();
                    toastr.success(status.response);
                    $('#submit_ingredient_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("submit" , "#add_product_form" , function(e) {
        e.preventDefault(); 
        var formData = $("#add_product_form").serialize();
        $.ajax({
            url:admin_url+'baking/save_product',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){ 
                if(status.msg=='success') {
                    $('#add_product_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_product_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on('click','.remove-option', function( e ) {
        e.preventDefault();
        var _this = $(this);
        var value = _this.attr('data-val');
        $('.option-row-'+value).remove();
    }); 

</script>


</body>
</html>