<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Feedbacks</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Feedbacks</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_feedback_modal">
                    <i class='fa fa-plus'></i> Add Feedback
                </button>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Feedbacks Activity list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="admin-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <th>Text</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($feedbacks as $feedback){ ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $feedback['text']; ?></td>
                                            <td>
                                                <?php if($feedback['status'] == 'pending') { ?>
                                                    <span class="label label-primary">Pending</span>
                                                <?php } elseif($feedback['status'] == 'success') { ?>
                                                    <span class="label label-success">Success</span>
                                                <?php } elseif($feedback['status'] == 'decline') { ?>
                                                    <span class="label label-danger">Decline</span>
                                                <?php } ?>
                                            </td>
                                            <td> <?php echo date('F jS, Y - h:i a' ,strtotime($feedback['created_at'])); ?> </td>  
                                            <td>
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $feedback['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_feedback_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add Feedback</h5>
                    </div>
                    <form method="post" id="add_feedback_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Feedback</label>
                                    <textarea class="form-control" id="feedback" name="feedback" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_feedback" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>
    </div>
</div>
<?php $this->load->view('common/admin_scripts'); ?>

<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>



</body>
</html>

<script>    
    $('#admin-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 0, 'desc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });
    $(document).on('click', '.delete-btn', function (event) {

        var id = $(this).attr('data-id');
        
        swal({
                title: "Are you sure?",
                text: "You want to delete this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'feedback/delete',
                        type:'post',
                        data:{ id : id },
                        dataType:'json',
                        success:function(status){
                            toastr.success(status.response, 'Success');
                            setTimeout(function () {
                                $(location).attr('href', admin_url+"feedback");
                            }, 2000);
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , "#submit_feedback" , function() {

        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_feedback_form").serialize();
        $.ajax({
            url:admin_url+'feedback/save',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_feedback_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_feedback_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
</script>

