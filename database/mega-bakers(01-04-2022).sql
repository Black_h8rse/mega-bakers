-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 01, 2022 at 07:57 AM
-- Server version: 8.0.27
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mega-bakers`
--

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
CREATE TABLE IF NOT EXISTS `ingredients` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `quantity` int NOT NULL,
  `created_by` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`id`, `name`, `quantity`, `created_by`, `created_at`) VALUES
(2, 'testing', 0, 1, '2022-03-06 19:07:12.066647'),
(3, '123', 0, 1, '2022-03-15 14:59:38.967021');

-- --------------------------------------------------------

--
-- Table structure for table `ingredients_meta`
--

DROP TABLE IF EXISTS `ingredients_meta`;
CREATE TABLE IF NOT EXISTS `ingredients_meta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ingredient_id` int NOT NULL,
  `pro_id` int NOT NULL,
  `quantity` int NOT NULL,
  `weight_type` int NOT NULL,
  `created_by` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ingredients_meta`
--

INSERT INTO `ingredients_meta` (`id`, `ingredient_id`, `pro_id`, `quantity`, `weight_type`, `created_by`) VALUES
(1, 2, 12, 123, 0, 1),
(2, 3, 1, 12, 0, 1),
(3, 3, 2, 123, 0, 1),
(4, 3, 4, 10, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(200) NOT NULL,
  `msg` longtext NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

DROP TABLE IF EXISTS `production`;
CREATE TABLE IF NOT EXISTS `production` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pro_id` int NOT NULL,
  `quantity` int NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `created_by` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`id`, `pro_id`, `quantity`, `status`, `created_at`, `created_by`) VALUES
(1, 13, 200, 1, '2022-03-15 14:16:52.373139', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `ingredient_id` int NOT NULL DEFAULT '0',
  `warehouse_id` int NOT NULL,
  `store_id` int NOT NULL,
  `product_type` int NOT NULL COMMENT '1 = warehouse,2=bakery',
  `type_id` int NOT NULL COMMENT '''Type'' table ID',
  `weight_type` int NOT NULL COMMENT '1=box,2=bag,3=liter,4=kg,5=gram',
  `weight` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `quantity` varchar(200) NOT NULL,
  `in_stock` varchar(200) NOT NULL,
  `paid_price` varchar(200) NOT NULL,
  `price_per_piece` int NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `created_by`, `ingredient_id`, `warehouse_id`, `store_id`, `product_type`, `type_id`, `weight_type`, `weight`, `quantity`, `in_stock`, `paid_price`, `price_per_piece`, `status`, `created_at`) VALUES
(20, 1, 2, 0, 0, 2, 13, 0, '', '', '', '', 0, 1, '2022-03-15 14:03:17.609676'),
(21, 1, 0, 8, 0, 1, 4, 3, '500', '500', '50', '123123123123', 246246246, 1, '2022-03-15 15:36:00.795511'),
(19, 1, 0, 7, 0, 1, 1, 1, '100', '100', '50', '123123', 1231, 1, '2022-03-15 13:53:09.075009');

-- --------------------------------------------------------

--
-- Table structure for table `product_meta`
--

DROP TABLE IF EXISTS `product_meta`;
CREATE TABLE IF NOT EXISTS `product_meta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pro_id` int NOT NULL,
  `pro_type_id` int NOT NULL,
  `warehouse_id` int NOT NULL,
  `store_id` int NOT NULL,
  `quantity` int NOT NULL,
  `weight_type` int NOT NULL COMMENT ' 	1=box,2=bag,3=liter,4=kg,5=gram 	',
  `in_stock` int NOT NULL,
  `detail` varchar(200) NOT NULL,
  `status` int NOT NULL COMMENT '1=success,2=pending,3=cancel',
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `created_by` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `product_meta`
--

INSERT INTO `product_meta` (`id`, `pro_id`, `pro_type_id`, `warehouse_id`, `store_id`, `quantity`, `weight_type`, `in_stock`, `detail`, `status`, `created_at`, `created_by`) VALUES
(22, 21, 4, 8, 7, 150, 3, 0, 'asd asd asd', 1, '2022-03-15 15:37:41.906422', 1),
(21, 19, 1, 7, 7, 50, 1, 0, 'sasda', 1, '2022-03-15 14:48:17.053629', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
CREATE TABLE IF NOT EXISTS `stores` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `warehouse_id` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `size` varchar(100) NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `created_by`, `warehouse_id`, `name`, `phone`, `size`, `status`, `created_at`) VALUES
(7, 1, 7, 'Philip Mccormick', '70', 'Unlimited', 1, '2022-02-20 19:40:21.746611');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `table_name` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `created_by`, `table_name`, `name`, `created_at`) VALUES
(1, 1, 'warehouse_product', 'Oil', '2022-01-01 08:15:52.559477'),
(2, 1, 'warehouse_product', 'Buckminster Banks', '2022-01-01 14:00:14.791763'),
(4, 1, 'warehouse_product', 'Ghee', '2022-01-16 15:44:49.223334'),
(6, 1, 'warehouse_product', 'sugar', '2022-01-18 12:17:01.393203'),
(13, 1, 'baking_products', 'product 1', '2022-03-15 14:03:17.609231');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `f_name` varchar(20) NOT NULL,
  `l_name` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `salary` int NOT NULL,
  `commission` varchar(200) NOT NULL,
  `created_by` int NOT NULL,
  `user_type` int NOT NULL COMMENT '1=superadmin,2=Admin,3=Ware-house manager,4=store manager,5=production manager',
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `f_name`, `l_name`, `email`, `password`, `phone`, `salary`, `commission`, `created_by`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'Hassan', 'Raza', 'raza.explorelogics@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+923447621846', 0, '', 0, 1, '2021-10-15 01:39:16.303218', '2022-01-01 12:39:20.345803'),
(26, 'Amery Bush', 'Olga Boyle', 'huhu@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '3', 0, '', 1, 2, '2022-01-01 12:09:16.630561', NULL),
(27, 'Ryder Coffey', 'Suki Hall', 'cajituqoqu@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '85', 0, '', 1, 3, '2022-01-01 12:10:04.392056', NULL),
(28, 'Tana Kim', 'Pearl Santos', 'piwufuky@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '97', 0, '', 1, 4, '2022-01-01 12:10:40.339356', NULL),
(29, 'Lilah Robbins', 'Chava Hebert', 'jofal@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '99', 0, '', 1, 5, '2022-01-01 12:11:03.645391', NULL),
(30, 'Merrill Alford', 'Nola Richmond', 'sajegewu@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '19', 20000, '2', 1, 5, '2022-01-01 13:19:02.651226', '2022-01-01 13:20:09.722499'),
(32, 'Kermit Kerr', 'Chaney Sharp', 'nucipanoxa@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '84', 29232, '10', 1, 6, '2022-01-01 13:38:14.979517', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `model` varchar(100) NOT NULL,
  `year` varchar(100) NOT NULL,
  `reg_no` varchar(100) NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `created_by`, `name`, `model`, `year`, `reg_no`, `status`, `created_at`) VALUES
(1, 1, 'Corolla', 'altus', '2017', 'AK 47', 1, '2022-01-01 13:29:13.570142');

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

DROP TABLE IF EXISTS `warehouses`;
CREATE TABLE IF NOT EXISTS `warehouses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `location` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type` int NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `warehouses`
--

INSERT INTO `warehouses` (`id`, `created_by`, `name`, `location`, `phone`, `size`, `type`, `status`, `created_at`, `updated_at`) VALUES
(7, 1, 'Sharon Mckinney', 'Ea laboriosam neque', '90', '48', 1, 1, '2022-01-30 10:57:50.092779', NULL),
(8, 1, 'Vielka Cleveland', 'Cumque autem ad cons', '44', '61', 1, 1, '2022-02-10 12:14:57.752512', NULL),
(10, 1, 'Jerry Cardenas', 'Illo quidem velit ma', '72', '', 1, 1, '2022-02-20 21:41:25.311530', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
