-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 06, 2022 at 12:16 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `explouv4_mega_bakers`
--

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `msg` longtext NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `url`, `msg`, `created_at`) VALUES
(1, 'Working on it', 'Hassan Raza added a vehicle registration number Sapiente sit duis no', '2022-01-01 13:59:03.009534'),
(2, 'Working on it', 'Hassan Raza add a new user Reagan Dennis Kyra Mayer', '2022-01-01 13:59:19.401972'),
(3, 'Working on it', 'Hassan Raza deleted a user ', '2022-01-01 13:59:27.934374'),
(4, 'Working on it', 'Hassan Raza deleted a vehicle ', '2022-01-01 13:59:33.822881'),
(5, 'Working on it', 'Hassan Raza created a warehouse Vielka Lucas', '2022-01-01 13:59:42.951904'),
(6, 'Working on it', 'Hassan Raza deleted a warehouse ', '2022-01-01 13:59:52.332672'),
(7, 'Working on it', 'Hassan Raza created a store 65', '2022-01-01 13:59:59.338855'),
(8, 'Working on it', 'Hassan Raza deleted a store ', '2022-01-01 14:00:07.695826'),
(9, 'Working on it', 'Hassan Raza created a new product type Buckminster Banks', '2022-01-01 14:00:14.791400'),
(10, 'Working on it', 'Hassan Raza created a new product 12123', '2022-01-01 14:00:33.848973'),
(11, 'Working on it', 'Hassan Raza deleted a product ', '2022-01-01 14:06:56.429330'),
(12, 'Working on it', 'Hassan Raza created a new product type type', '2022-01-13 06:15:38.074089'),
(13, 'Working on it', 'Hassan Raza created a new product 200', '2022-01-13 06:16:20.713312'),
(14, 'Working on it', 'Hassan Raza deleted a product ', '2022-01-13 06:16:37.665127'),
(15, 'Working on it', 'Hassan Raza created a new product type Ghee', '2022-01-16 15:44:49.220905'),
(16, 'Working on it', 'Hassan Raza created a new product 200', '2022-01-16 15:45:14.804694'),
(17, 'Working on it', 'Hassan Raza created a new product type ghee', '2022-01-18 12:15:59.419071'),
(18, 'Working on it', 'Hassan Raza created a new product type sugar', '2022-01-18 12:17:01.381093'),
(19, 'Working on it', 'Hassan Raza created a new product 40', '2022-01-18 12:17:24.639411'),
(20, 'Working on it', 'Hassan Raza deleted a product ', '2022-01-18 12:17:43.926427'),
(21, 'Working on it', 'Hassan Raza deleted a warehouse ', '2022-01-30 10:57:42.049978'),
(22, 'Working on it', 'Hassan Raza created a warehouse Sharon Mckinney', '2022-01-30 10:57:50.087160'),
(23, 'Working on it', 'Hassan Raza deleted a store ', '2022-01-30 10:58:00.976239'),
(24, 'Working on it', 'Hassan Raza deleted a store ', '2022-01-30 10:58:04.717536'),
(25, 'Working on it', 'Hassan Raza deleted a store ', '2022-01-30 10:58:08.471642'),
(26, 'Working on it', 'Hassan Raza deleted a product ', '2022-01-30 10:58:13.933421'),
(27, 'Working on it', 'Hassan Raza deleted a product ', '2022-01-30 10:58:18.082312'),
(28, 'Working on it', 'Hassan Raza deleted a product ', '2022-01-30 10:58:21.927482'),
(29, 'Working on it', 'Hassan Raza deleted a product ', '2022-01-30 10:58:26.315610'),
(30, 'Working on it', 'Hassan Raza created a store 62', '2022-02-10 12:14:43.394548'),
(31, 'Working on it', 'Hassan Raza deleted a store ', '2022-02-10 12:14:48.950271'),
(32, 'Working on it', 'Hassan Raza created a warehouse Vielka Cleveland', '2022-02-10 12:14:57.747601'),
(33, 'Working on it', 'Hassan Raza created a warehouse Tatyana Valdez', '2022-02-17 14:20:28.756329'),
(34, 'Working on it', 'Hassan Raza deleted a warehouse ', '2022-02-17 14:20:36.647145'),
(35, 'Working on it', 'Hassan Raza created a new product type for warehouse products testing one more time', '2022-02-17 15:06:56.084611'),
(36, 'Working on it', 'Hassan Raza purchase a new product in warehouse 548', '2022-02-17 15:35:39.992566'),
(37, 'Working on it', 'Hassan Raza purchase a new product in warehouse 548', '2022-02-17 15:43:30.886353'),
(38, 'Working on it', 'Hassan Raza purchase a new product in warehouse 336', '2022-02-17 15:53:54.067341'),
(39, 'Working on it', 'Hassan Raza deleted a warehouse ', '2022-02-17 15:57:49.212846'),
(40, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-17 17:34:14.830290'),
(41, 'Working on it', 'Hassan Raza deleted a product from warehouse ', '2022-02-17 17:34:22.218261'),
(42, 'Working on it', 'Hassan Raza deleted a product from warehouse ', '2022-02-20 18:43:06.654526'),
(43, 'Working on it', 'Hassan Raza purchase a new product in warehouse 200', '2022-02-20 18:43:28.936521'),
(44, 'Working on it', 'Hassan Raza purchase a new product in warehouse 100', '2022-02-20 18:54:38.428896'),
(45, 'Working on it', 'Hassan Raza created a store ', '2022-02-20 19:40:21.739924'),
(46, 'Working on it', 'Hassan Raza created a store ', '2022-02-20 19:40:33.620427'),
(47, 'Working on it', 'Hassan Raza created a store ', '2022-02-20 19:41:26.614024'),
(48, 'Working on it', 'Hassan Raza created a store ', '2022-02-20 19:44:54.849534'),
(49, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:12:55.009643'),
(50, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:13:40.171958'),
(51, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:14:28.826937'),
(52, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:15:13.995102'),
(53, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store (  )', '2022-02-20 21:27:33.599888'),
(54, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store (  )', '2022-02-20 21:29:45.101590'),
(55, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store (  )', '2022-02-20 21:31:01.148106'),
(56, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store (  )', '2022-02-20 21:31:26.050595'),
(57, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:31:46.706560'),
(58, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:32:40.678312'),
(59, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:39:02.394783'),
(60, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:40:39.041961'),
(61, 'Working on it', 'Hassan Raza created a warehouse Jerry Cardenas', '2022-02-20 21:41:25.311189'),
(62, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:44:18.115137'),
(63, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:45:34.401493'),
(64, 'Working on it', 'Hassan Raza purchase a new product in warehouse 213123123', '2022-02-20 21:47:17.579064'),
(65, 'Working on it', 'Hassan Raza created a new product type for warehouse products asdasd', '2022-02-20 21:47:27.445587'),
(66, 'Working on it', 'Hassan Raza deleted a product from warehouse ', '2022-02-20 21:47:34.321033'),
(67, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-20 21:47:39.137982'),
(68, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:48:19.172998'),
(69, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:50:25.916506'),
(70, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-20 21:50:37.991968'),
(71, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick asdsad )', '2022-02-20 22:02:43.997716'),
(72, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick asdsad )', '2022-02-20 22:05:13.047707'),
(73, 'Working on it', 'Hassan Raza created a warehouse Iliana Carey', '2022-02-21 04:56:53.944709'),
(74, 'Working on it', 'Hassan Raza deleted a warehouse ', '2022-02-21 04:57:01.597671'),
(75, 'Working on it', 'Hassan Raza created a new product type for warehouse products test', '2022-02-21 04:57:16.114037'),
(76, 'Working on it', 'Hassan Raza purchase a new product in warehouse 200', '2022-02-21 04:57:34.271867'),
(77, 'Working on it', 'Hassan Raza transfer product from warehouse ( Sharon Mckinney ) to store ( Philip Mccormick )', '2022-02-21 04:58:01.359416'),
(78, 'Working on it', 'Hassan Raza deleted a warehouse ', '2022-02-21 11:01:13.716129'),
(79, 'Working on it', 'Hassan Raza deleted a warehouse ', '2022-02-21 11:01:19.118360'),
(80, 'Working on it', 'Hassan Raza deleted a warehouse ', '2022-02-21 11:01:24.652947'),
(81, 'Working on it', 'Hassan Raza created a warehouse MAIN WAREHOUSE', '2022-02-21 11:01:45.462254'),
(82, 'Working on it', 'Hassan Raza deleted a product from warehouse ', '2022-02-21 11:01:59.695424'),
(83, 'Working on it', 'Hassan Raza deleted a product from warehouse ', '2022-02-21 11:02:04.941832'),
(84, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 11:02:15.109953'),
(85, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 11:02:21.414154'),
(86, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 11:02:27.699381'),
(87, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 11:02:28.655495'),
(88, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 11:02:37.520952'),
(89, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 11:02:43.646289'),
(90, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 11:02:48.132767'),
(91, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 11:02:54.322077'),
(92, 'Working on it', 'Hassan Raza created a new product type for warehouse products Flour', '2022-02-21 11:08:44.975118'),
(93, 'Working on it', 'Hassan Raza purchase a new product in warehouse 500', '2022-02-21 11:09:10.783438'),
(94, 'Working on it', 'Hassan Raza created a new product type for warehouse products Sugar', '2022-02-21 11:11:01.944540'),
(95, 'Working on it', 'Hassan Raza purchase a new product in warehouse 100', '2022-02-21 11:11:37.689009'),
(96, 'Working on it', 'Hassan Raza deleted a product from warehouse ', '2022-02-21 11:12:57.341703'),
(97, 'Working on it', 'Hassan Raza deleted a product from warehouse ', '2022-02-21 11:13:03.429856'),
(98, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 11:13:20.440316'),
(99, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 11:13:26.365899'),
(100, 'Working on it', 'Hassan Raza created a new product type for warehouse products test', '2022-02-21 11:58:12.253158'),
(101, 'Working on it', 'Hassan Raza purchase a new product in warehouse 200', '2022-02-21 11:59:05.044112'),
(102, 'Working on it', 'Hassan Raza created a new product type for warehouse products fat25kgs', '2022-02-21 11:59:58.515551'),
(103, 'Working on it', 'Hassan Raza created a new product type for warehouse products BAKERY', '2022-02-21 12:01:47.567915'),
(104, 'Working on it', 'Hassan Raza deleted a product from warehouse ', '2022-02-21 12:14:54.774883'),
(105, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 12:15:01.321948'),
(106, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 12:15:08.536549'),
(107, 'Working on it', 'Hassan Raza deleted a product type ', '2022-02-21 12:15:15.543289'),
(108, 'Working on it', 'Hassan Raza created a new product type for warehouse products SALIMA SUGAR', '2022-02-21 12:15:36.757466'),
(109, 'Working on it', 'Hassan Raza created a new product type for warehouse products FLOUR 25KGS', '2022-02-21 12:16:11.599208'),
(110, 'Working on it', 'Hassan Raza created a new product type for warehouse products CAKE FLOUR', '2022-02-21 12:16:23.610533'),
(111, 'Working on it', 'Hassan Raza created a new product type for warehouse products COOKING OIL', '2022-02-21 12:16:45.274230'),
(112, 'Working on it', 'Hassan Raza purchase a new product in warehouse 300', '2022-02-21 12:18:24.622443'),
(113, 'Working on it', 'Hassan Raza deleted a store ', '2022-02-21 12:19:03.575497'),
(114, 'Working on it', 'Hassan Raza deleted a store ', '2022-02-21 12:19:10.227985'),
(115, 'Working on it', 'Hassan Raza created a store ', '2022-02-21 12:20:44.792593'),
(116, 'Working on it', 'Hassan Raza transfer product from warehouse ( MAIN WAREHOUSE ) to store ( WAREHOUSE )', '2022-02-21 12:22:01.896073'),
(117, 'Working on it', 'Hassan Raza purchase a new product in warehouse 100', '2022-02-21 16:18:01.929809'),
(118, 'Working on it', 'Hassan Raza purchase a new product in warehouse 100', '2022-02-21 16:18:59.645132'),
(119, 'Working on it', 'Hassan Raza deleted a product from warehouse ', '2022-02-21 16:19:15.692075'),
(120, 'Working on it', 'Hassan Raza purchase a new product in warehouse 100', '2022-02-21 16:19:32.243765'),
(121, 'Working on it', 'Hassan Raza transfer product from warehouse ( MAIN WAREHOUSE ) to store ( WAREHOUSE )', '2022-02-21 16:24:37.765658');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `product_type` int(11) NOT NULL COMMENT '1 = warehouse,2=bakery',
  `type_id` int(11) NOT NULL,
  `weight_type` int(11) NOT NULL COMMENT '1=box,2=bag,3=kg,4=gram,5=leter',
  `weight` varchar(100) NOT NULL,
  `quantity` varchar(200) NOT NULL,
  `in_stock` varchar(200) NOT NULL,
  `paid_price` varchar(200) NOT NULL,
  `price_per_piece` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `created_by`, `warehouse_id`, `store_id`, `product_type`, `type_id`, `weight_type`, `weight`, `quantity`, `in_stock`, `paid_price`, `price_per_piece`, `status`, `created_at`) VALUES
(18, 1, 12, 0, 1, 17, 1, '300', '300', '0', '100', 0, 1, '2022-02-21 12:18:24.622870'),
(19, 1, 12, 0, 1, 17, 1, '100', '100', '100', '30000', 300, 1, '2022-02-21 16:18:01.930171'),
(14, 1, 7, 0, 1, 9, 2, '200', '200', '0', '13000', 65, 1, '2022-02-21 04:57:34.272148'),
(21, 1, 12, 0, 1, 17, 1, '100', '100', '100', '30000', 300, 1, '2022-02-21 16:19:32.244091');

-- --------------------------------------------------------

--
-- Table structure for table `product_meta`
--

CREATE TABLE `product_meta` (
  `id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `pro_type_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `detail` varchar(200) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=success,2=pending,3=cancel',
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_meta`
--

INSERT INTO `product_meta` (`id`, `pro_id`, `pro_type_id`, `warehouse_id`, `store_id`, `quantity`, `detail`, `status`, `created_at`, `created_by`) VALUES
(19, 11, 4, 7, 8, 100, 'asasd asdasd', 1, '2022-02-20 22:05:13.048893', 1),
(20, 14, 9, 7, 7, 200, 'asd asd', 1, '2022-02-21 04:58:01.360981', 1),
(21, 18, 17, 12, 11, 100, 'PRODUCTION', 1, '2022-02-21 12:22:01.905721', 1),
(22, 18, 17, 12, 11, 200, 'SALE', 1, '2022-02-21 16:24:37.766273', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `created_by`, `warehouse_id`, `name`, `phone`, `size`, `status`, `created_at`) VALUES
(11, 1, 12, 'WAREHOUSE', '0', 'Unlimited', 1, '2022-02-21 12:20:44.792855');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `table_name` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `created_by`, `table_name`, `name`, `created_at`) VALUES
(18, 1, 'warehouse_product', 'COOKING OIL', '2022-02-21 12:16:45.274466'),
(17, 1, 'warehouse_product', 'CAKE FLOUR', '2022-02-21 12:16:23.610837'),
(16, 1, 'warehouse_product', 'FLOUR 25KGS', '2022-02-21 12:16:11.599645'),
(15, 1, 'warehouse_product', 'SALIMA SUGAR', '2022-02-21 12:15:36.757803');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `f_name` varchar(20) NOT NULL,
  `l_name` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `salary` int(11) NOT NULL,
  `commission` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1=superadmin,2=Admin,3=Ware-house manager,4=store manager,5=production manager',
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `f_name`, `l_name`, `email`, `password`, `phone`, `salary`, `commission`, `created_by`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'Hassan', 'Raza', 'raza.explorelogics@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+923447621846', 0, '', 0, 1, '2021-10-15 01:39:16.303218', '2022-01-01 12:39:20.345803'),
(26, 'Amery Bush', 'Olga Boyle', 'huhu@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '3', 0, '', 1, 2, '2022-01-01 12:09:16.630561', NULL),
(27, 'Ryder Coffey', 'Suki Hall', 'cajituqoqu@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '85', 0, '', 1, 3, '2022-01-01 12:10:04.392056', NULL),
(28, 'Tana Kim', 'Pearl Santos', 'piwufuky@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '97', 0, '', 1, 4, '2022-01-01 12:10:40.339356', NULL),
(29, 'Lilah Robbins', 'Chava Hebert', 'jofal@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '99', 0, '', 1, 5, '2022-01-01 12:11:03.645391', NULL),
(30, 'Merrill Alford', 'Nola Richmond', 'sajegewu@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '19', 20000, '2', 1, 5, '2022-01-01 13:19:02.651226', '2022-01-01 13:20:09.722499'),
(32, 'Kermit Kerr', 'Chaney Sharp', 'nucipanoxa@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '84', 29232, '10', 1, 6, '2022-01-01 13:38:14.979517', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `model` varchar(100) NOT NULL,
  `year` varchar(100) NOT NULL,
  `reg_no` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `created_by`, `name`, `model`, `year`, `reg_no`, `status`, `created_at`) VALUES
(1, 1, 'Corolla', 'altus', '2017', 'AK 47', 1, '2022-01-01 13:29:13.570142');

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

CREATE TABLE `warehouses` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `location` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `warehouses`
--

INSERT INTO `warehouses` (`id`, `created_by`, `name`, `location`, `phone`, `size`, `type`, `status`, `created_at`, `updated_at`) VALUES
(12, 1, 'MAIN WAREHOUSE', '0', '0', '', 1, 1, '2022-02-21 11:01:45.462638', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_meta`
--
ALTER TABLE `product_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `product_meta`
--
ALTER TABLE `product_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `warehouses`
--
ALTER TABLE `warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
